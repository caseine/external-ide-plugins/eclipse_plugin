/*
 * Copyright 2018, 2019: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package caseine.vpl.nature;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.window.Window;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import caseine.vpl.properties.PropertyConstants;
import caseine.vpl.swt.VplIDDialog;

/**
 * 
 * @author Christophe Saint-Marcel
 * @author Astor Bizard
 */
public class CaseineNature implements IProjectNature {
	
	public static final String NATURE_ID = "CaseineVPL.caseineNature";

	private IProject project;

	@Override
	public void configure() throws CoreException {
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		
		String vplID = project.getPersistentProperty(PropertyConstants.VPLID_PROPERTY);
		if (vplID == null || vplID.equals("")) {
			VplIDDialog dialog = new VplIDDialog(window.getShell());
			dialog.open();
			if(dialog.getReturnCode() == Window.OK)
				vplID = dialog.getVPLID();
			else
				return;
		}
		
		project.setPersistentProperty(PropertyConstants.VPLID_PROPERTY, vplID);
	}

	@Override
	public void deconfigure() throws CoreException {
		// Nothing to do, keep the VPLID property stored for eventual future re-configure().
	}

	@Override
	public IProject getProject() {
		return project;
	}

	@Override
	public void setProject(IProject project) {
		this.project = project;
	}
}
