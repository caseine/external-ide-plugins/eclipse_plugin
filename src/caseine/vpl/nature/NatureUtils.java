/*
 * Copyright 2018, 2019: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package caseine.vpl.nature;

import java.util.Arrays;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.ui.IPackagesViewPart;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.ui.PlatformUI;

/**
 * 
 * @author Astor Bizard
 * @author Christophe Saint-Marcel
 */
public final class NatureUtils {

	private NatureUtils() {
		throw new IllegalStateException("Utility class");
	}
	
	/**
	 * Toggles specified nature on a project (enables it if not enabled, disables it if enabled).
	 * @param project The project to have the nature added or removed
	 */
	public static void toggleNature(IProject project, String natureID) throws CoreException {
		String[] natures = project.getDescription().getNatureIds();
		int i=0;
		while(i<natures.length && !natures[i].equals(natureID))
			i++;
		String[] newNatures;
		if (i==natures.length) {
			newNatures = Arrays.copyOf(natures, natures.length+1);
			newNatures[natures.length] = natureID;
		}
		else {
			natures[i] = natures[natures.length-1];
			newNatures = Arrays.copyOf(natures, natures.length-1);
		}
		
		applyNatures(project, newNatures);
		
		// Refresh display
		refreshPackageExplorer();
	}

	/**
	 * A trick to refresh the package explorer (project.refreshLocal() doesn't refresh its appearance in package explorer)
	 * @see <a href=https://www.eclipse.org/forums/index.php/t/376196/>Topic on eclipse forum</a>
	 */
	private static void refreshPackageExplorer() {
		final IPackagesViewPart packageExplorerView = (IPackagesViewPart) PlatformUI.getWorkbench().
				getActiveWorkbenchWindow().getActivePage().findView(JavaUI.ID_PACKAGES);
		if (packageExplorerView!=null)
			packageExplorerView.getTreeViewer().refresh();
	}
	
	private static boolean applyNatures(IProject project, String[] natures) throws CoreException {
		IProjectDescription description = project.getDescription();
		if (ResourcesPlugin.getWorkspace().validateNatureSet(natures).getCode() == IStatus.OK) {
		    description.setNatureIds(natures);
		    project.setDescription(description, null);
		    return true;
		}
		else
			return false;
	}
	
	/**
	 * Adds the specified natures to the specified project.
	 * This doesn't check if the natures are already present ; caller must ensure that the project doesn't already have these natures.
	 * @param project
	 * @param natures
	 */
	public static void addNatures(IProject project, String... natures) throws CoreException {
		String[] oldNatures = project.getDescription().getNatureIds();
		String[] newNatures = Arrays.copyOf(oldNatures, oldNatures.length);
		for (String nature : natures) {
			String[] tmpNatures = Arrays.copyOf(newNatures, newNatures.length + 1);
			tmpNatures[tmpNatures.length - 1] = nature;
			if (ResourcesPlugin.getWorkspace().validateNatureSet(tmpNatures).getCode() == IStatus.OK) {
				newNatures = tmpNatures;
			}
		}
		if (newNatures.length > oldNatures.length) {
			applyNatures(project, newNatures);
		}
	}
}
