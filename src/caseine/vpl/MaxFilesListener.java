/*
 * Copyright 2018, 2019: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Universit� Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package caseine.vpl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.WorkspaceJob;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;

import caseine.vpl.handlers.CaseineHandler;
import caseine.vpl.nature.CaseineNature;
import caseine.vpl.properties.PropertyConstants;
import vplwsclient.FileUtils;
import vplwsclient.exception.MoodleWebServiceException;
import vplwsclient.exception.VplConnectionException;

/**
 * Workspace change listener.
 * Displays a warning if the user creates a file beyond the maximum number of
 * files allowed in the VPL linked to a project.
 * 
 * @author Astor Bizard, Titouan Mazier
 */
public class MaxFilesListener implements IResourceChangeListener {

	private Shell shell;

	public MaxFilesListener(Shell parent) {
		shell = parent;
	}

	/**
	 * Regenerate (replace if it exists) .vplignore file at project root.
	 * 
	 * @param projectPath The path to the project directory.
	 */
	public static void generateVplignore(String projectPath) throws IOException {
		MaxFilesListener.generateVplignore(projectPath, true);
	}

	/**
	 * Regenerate .vplignore file at project root.
	 * 
	 * @param projectPath The path to the project directory.
	 * @param replaceExisting If false and the file already exists, nothing will be done.
	 */
	public static void generateVplignore(String projectPath, boolean replaceExisting) throws IOException {
		String filePath = projectPath + File.separator + ".vplignore";
		if (replaceExisting || !new File(filePath).exists()) {
			// Generate vplignore file
			try (InputStream sourceStream = MaxFilesListener.class.getResourceAsStream("/vplignoreTemplate")) {
				Files.copy(sourceStream, Paths.get(filePath), StandardCopyOption.REPLACE_EXISTING);
			}
		}
	}

	public static void displayMaxFileDialog(Shell shell, IProject project, String dialogName, String message, String buttonName) {
		// Open custom dialog
		int result = new MessageDialog(shell, dialogName, null, message,
				MessageDialog.ERROR, new String[] { "Add files to .vplignore", buttonName }, 1)
				.open();

		if (result == 0) {
			// Open .vplignore file
			new WorkspaceJob("Creating and opening .vplignore") {
				@Override
				public IStatus runInWorkspace(IProgressMonitor monitor) throws CoreException {
					Display.getDefault().syncExec(() -> {
						try {
							generateVplignore(project.getLocation().toOSString(), false);
							project.refreshLocal(IResource.DEPTH_ONE, null);
							IDE.openEditor(
									PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(),
									project.getFile(".vplignore"));
						} catch (CoreException | IOException e) {
							Display.getDefault().syncExec(() ->
								ErrorDisplayer.displayAsDialog(e, shell,
												"Error while " + (e instanceof IOException ? "creating" : "opening") + " .vplignore"));
						}
					});
					return Status.OK_STATUS;
				}
			}.schedule();
		}
	}

	/**
	 * Called every time a resource is changed. Check if file has been added.
	 * If so, check if the number of file does not exceed the vpl limits, if not
	 * display a dialog window to warn the user.
	 */
	@Override
	public void resourceChanged(IResourceChangeEvent event) {
		try {
			IResourceDelta deltaTree = event.getDelta();

			class ResourceVisitor implements IResourceDeltaVisitor {
				private boolean foundAddition = false;
				private IProject project = null;
				
				@Override
				public boolean visit(IResourceDelta delta) throws CoreException {
					project = delta.getResource().getProject();
					if (project == null) // We are not in a project yet - continue
						return !foundAddition;

					if (!project.exists() || !project.hasNature(CaseineNature.NATURE_ID)
							|| project.getPersistentProperty(PropertyConstants.VPLID_PROPERTY) == null)
						return false; // We are in a non-Caseine project (or just deleted project - or old caseine
										// project that didnt finish to re-initialize) - stop

					if (delta.getKind() == IResourceDelta.ADDED) {
						// We are on an added resource in a caseine project
						String resourcePath = delta.getResource().getProjectRelativePath().toString();
						int folderNameLength = resourcePath.indexOf("/");
						if (folderNameLength > 0
								&& CaseineHandler.allFolders.contains(resourcePath.substring(0, folderNameLength))) {
							// The resource was added in a folder that is exported by this plugin and thus
							// will count towards max files - mark as found and stop
							foundAddition = true;
							return false;
						}
					}
					return !foundAddition; // Continue
				}
			}
			
			// Visit the tree of all changes (a change on a file affects its parents)
			ResourceVisitor visitor = new ResourceVisitor();
			deltaTree.accept(visitor);
			
			if (visitor.foundAddition) {
				// We found an added file - count files on this project and max files from vpl
				String projectPath = visitor.project.getLocation().toOSString();
				List<String> excludedFiles = FileUtils.scanExcludedList(projectPath);
				int nbFiles = FileUtils.countIncludedFiles(projectPath, CaseineHandler.allFolders, projectPath, excludedFiles);
				
				int maxFiles = WebserviceClientFactory.createFromProject(visitor.project).callService("mod_vpl_info").getInt("maxfiles");
				if (nbFiles > maxFiles) {
					// Create a GUI delegate thread (necessary to display a dialog)
					Display.getDefault().syncExec(() -> displayMaxFileDialog(shell, visitor.project, "Caseine",
							"You are creating more files in your project than allowed by the VPL ("
									+ nbFiles + "/" + maxFiles + ")\n"
									+ "You will not be able to save or submit your work !",
							"Understood"));
				}
			}
		} catch (CoreException | MoodleWebServiceException | VplConnectionException e) {
			// An error occured - the operation is not crucial, let it abort.
			Activator.log(e);
		}
	}
}
