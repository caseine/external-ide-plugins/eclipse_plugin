/*
 * Copyright 2018, 2019: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package caseine.vpl;

import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import caseine.vpl.marker.EditorFilesListener;

/**
 * The activator class controls the plug-in life cycle.
 * @author Christophe Saint-Marcel
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "CaseineVPL"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;
	
	private IWorkbenchWindow window;
	private EditorFilesListener editorFilesListener;
	private MaxFilesListener maxFilesListener;
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		
		window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();

		editorFilesListener = new EditorFilesListener();
		window.getPartService().addPartListener(editorFilesListener);
		
		maxFilesListener = new MaxFilesListener(window.getShell());
		ResourcesPlugin.getWorkspace().addResourceChangeListener(maxFilesListener,IResourceChangeEvent.POST_CHANGE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		window.getPartService().removePartListener(editorFilesListener);
		ResourcesPlugin.getWorkspace().removeResourceChangeListener(maxFilesListener);
		super.stop(context);
	}

	/**
	 * Returns the shared instance (ie. this plugin's Activator instance).
	 */
	public static Activator getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given plugin-relative path.
	 * @param path The plugin-relative path of the image to retrieve.
	 * @return The image descriptor.
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
	
	/**
	 * Logs a message with specified status in the workspace log.
	 * @param status The status of this message. Should be a constant from {@link Status}.
	 * @param message The message to log.
	 */
	public static void log(int status, String message){
		plugin.getLog().log(new Status(status, PLUGIN_ID, message));
	}
	
	/**
	 * Logs a message linked with an exception in the workspace log, with error status.
	 * @param message The message to log.
	 * @param exception The exception to log.
	 */
	public static void log(String message, Throwable exception){
		plugin.getLog().log(new Status(Status.ERROR, PLUGIN_ID, message, exception));
	}
	
	/**
	 * Logs a {@code Throwable} with a common message in the workspace log, with error status.
	 * @param exception The exception to log.
	 */
	public static void log(Throwable exception) {
		String message;
		if (exception instanceof CoreException) {
			message = "An error occured while accessing project or workbench data.";
		} else {
			message = exception.getLocalizedMessage();
		}
		log(message, exception);
	}
}
