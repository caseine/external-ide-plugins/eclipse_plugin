/*
 * Copyright 2018, 2019: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package caseine.vpl;import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.preference.IPreferenceStore;

import caseine.vpl.preferences.PreferenceConstants;
import caseine.vpl.properties.PropertyConstants;
import vplwsclient.RestJsonMoodleClient;

/**
 * A factory to build web-service clients.
 * @author Astor Bizard
 */public final class WebserviceClientFactory {
	
	private WebserviceClientFactory() {
		throw new IllegalStateException("Factory class");
	}
	
	/**
	 * Builds a {@link RestJsonMoodleClient} from an IProject, using token and URL stored in eclipse preferences.
	 */
	public static RestJsonMoodleClient createFromProject(IProject project) {
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		String url = store.getString(PreferenceConstants.P_URL);
		String token = store.getString(PreferenceConstants.P_SECURITY_KEY);
		String vplID;
		String vplPassword;
		try {
			vplID = project.getPersistentProperty(PropertyConstants.VPLID_PROPERTY);
			if(vplID == null)
				vplID = "";
			vplPassword = project.getPersistentProperty(PropertyConstants.VPLPASSWORD_PROPERTY);
			if(vplPassword == null)
				vplPassword = "";
		} catch (CoreException e) {
			Activator.log(e);
			vplID = "";
			vplPassword = "";
		}
		return new RestJsonMoodleClient(vplID, token, url, vplPassword);
	}}
