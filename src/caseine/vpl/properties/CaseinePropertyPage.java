/*
 * Copyright 2018, 2019: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package caseine.vpl.properties;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.PropertyPage;

import caseine.vpl.Activator;
import caseine.vpl.swt.AlphaNumText;

/**
 * Manages the properties of a Caseine project.
 * @author Christophe Saint-Marcel
 * @author Astor Bizard
 */
public class CaseinePropertyPage extends PropertyPage {

	private AlphaNumText vplIDText;
	private Text passwordText;
	
	private IProject project;

	/**
	 * @see PreferencePage#createContents(Composite)
	 */
	protected Control createContents(Composite parent) {
		
		project = getElement().getAdapter(IProject.class);
		
		Composite container = new Composite(parent, SWT.NONE);
		
		// Layout with 2 items per line (<label :> [text field])
		container.setLayout(new GridLayout(2,false));
		
		vplIDText = new AlphaNumText(container, "&VPL ID: ", "VPL ID", 2, true);

		vplIDText.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
            	CaseinePropertyPage.this.setValid(vplIDText.isValid());
            }
        });
		
		new Label(container, SWT.NONE).setText("&Password: ");
		passwordText = new Text(container, SWT.BORDER | SWT.PASSWORD);
		passwordText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		
		// Fill the text fields with current properties
		try {
			vplIDText.setText(project.getPersistentProperty(PropertyConstants.VPLID_PROPERTY));
			if(project.getPersistentProperty(PropertyConstants.VPLPASSWORD_PROPERTY) != null) {
				passwordText.setText(project.getPersistentProperty(PropertyConstants.VPLPASSWORD_PROPERTY));
			}
		} catch (CoreException e) {
			Activator.log(e);
		}
		
		
		return container;
	}
	
	public boolean performOk() {
		try {
			project.setPersistentProperty(PropertyConstants.VPLID_PROPERTY, vplIDText.getText());
			project.setPersistentProperty(PropertyConstants.VPLPASSWORD_PROPERTY, passwordText.getText());
		} catch (CoreException e) {
			Activator.log(e);
			return false;
		}
		return true;
	}

}