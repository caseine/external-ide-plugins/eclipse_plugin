/*
 * Copyright 2018, 2019: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package caseine.vpl.properties;

import org.eclipse.core.runtime.QualifiedName;

/**
 * @author Christophe Saint-Marcel
 * @author Astor Bizard
 */
public class PropertyConstants {

	public static final QualifiedName VPLID_PROPERTY = new QualifiedName("", "VPL_ID");
	public static final QualifiedName VPLPASSWORD_PROPERTY = new QualifiedName("", "VPL_PASSWORD");

	private PropertyConstants() {
		throw new IllegalStateException("Utility class");
	}
}
