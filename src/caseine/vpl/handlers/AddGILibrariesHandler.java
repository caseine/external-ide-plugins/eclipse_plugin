/*
 * Copyright 2018, 2019: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package caseine.vpl.handlers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;

import caseine.vpl.Activator;
import caseine.vpl.ErrorDisplayer;
import caseine.vpl.WorkbenchUtils;

public class AddGILibrariesHandler extends AbstractHandler {

	/**
	 * The folder in which the GI libraries will be stored within the workspace.
	 */
	public static final String GILibFolder = "libGI";
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Shell shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();
		ISelection selectedObject = HandlerUtil.getCurrentSelection(event);
		IProject project = WorkbenchUtils.getProjectFromSelection(selectedObject);

		if (project != null) {
			try {
				addGILibraries(JavaCore.create(project));
				project.refreshLocal(IResource.DEPTH_INFINITE, null);
				MessageDialog.openInformation(shell, "Caseine", "GI libraries successfully added to project");
			} catch (CoreException | IOException e) {
				String message = "An error occured while ";
				if(e instanceof CoreException) {
					Activator.log((CoreException)e);
					message += "linking GI libraries to project";
				}
				else {
					message += "writing GI libraries to disk";
				}
				ErrorDisplayer.displayAsDialog(e, shell, message);
			}
		}
		return null;
	}
	
	private static final List<String> GILibs = Arrays.asList("StdDraw.class","StdDrawLife.class","LifeThread.class","CreationWindow.class");

	/**
	 * Adds references to the GI libraries for the specified project. The libraries are stored in {@code workspace}/{@link #GILibFolder}.
	 * Calling this method refreshes the files in that folder.
	 * @param javaProject - the project to which the libraries need to be added
	 * @throws CoreException if the libraries could not be linked to the project
	 * @throws IOException if the libraries could not be written to disk
	 */
	public static void addGILibraries(IJavaProject javaProject) throws CoreException, IOException {
		IPath localGIFolder = javaProject.getProject().getWorkspace().getRoot().getLocation().append(GILibFolder);
		
		if(!localGIFolder.toFile().exists() && !localGIFolder.toFile().mkdirs())
			throw new IOException();
		
		// Add the GI libraries location to classpath
		ArrayList<IClasspathEntry> entries = new ArrayList<>(Arrays.asList(javaProject.getRawClasspath()));
		IClasspathEntry libEntry = JavaCore.newLibraryEntry(localGIFolder, null, null);
		if(!entries.contains(libEntry)) {
			entries.add(libEntry);
			javaProject.setRawClasspath(entries.toArray(new IClasspathEntry[0]), true, null);
		}
		
		// Copy the GI libraries to <workspace>/<GILibFolder>
		for(String GILib : GILibs) {
			InputStream GILibStream = FileLocator.openStream(Activator.getDefault().getBundle(), new Path("libInfoGI" + File.separator + GILib), false);
			try (OutputStream targetStream = new FileOutputStream(new File(localGIFolder + File.separator + GILib))){
				byte[] buffer = new byte[4096];
				int read;
			    while ((read = GILibStream.read(buffer)) > 0) {
			    	targetStream.write(buffer, 0, read);
			    }
			}
		}
	}
}
