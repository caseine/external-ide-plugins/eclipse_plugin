/*
 * Copyright 2018, 2019: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Universit� Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package caseine.vpl.handlers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.common.NotDefinedException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import caseine.vpl.Activator;
import caseine.vpl.ErrorDisplayer;
import caseine.vpl.MaxFilesListener;
import caseine.vpl.WebserviceClientFactory;
import caseine.vpl.WorkbenchUtils;
import caseine.vpl.nature.CaseineNature;
import caseine.vpl.properties.PropertyConstants;
import caseine.vpl.swt.PasswordDialog;
import caseine.vpl.wizard.CaseineProjectWizard;
import caseine.vpl.wizard.CaseineProjectWizard.ProjectType;
import vplwsclient.FileUtils;
import vplwsclient.RestJsonMoodleClient;
import vplwsclient.VplFile;
import vplwsclient.exception.MaxFilesException;
import vplwsclient.exception.MoodleWebServiceException;
import vplwsclient.exception.PasswordException;
import vplwsclient.exception.VplConnectionException;
import vplwsclient.exception.VplException;

/**
 * The main abstract handler extends AbstractHandler, an IHandler base class.
 * It can be specialized to implement specific Caseine commands like import, save, ...
 * 
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 * @author Christophe Saint-Marcel
 */
public abstract class CaseineHandler extends AbstractHandler {

    private static final Logger LOGGER = Logger.getLogger( CaseineHandler.class.getPackage().getName() );
    
	/**
	 * The Caseine client.
	 */
	protected RestJsonMoodleClient moodleClient;

	/**
	 * To display the dialogs.
	 */
	protected Shell shell;

	/**
	 * The Eclipse project.
	 */
	protected IProject project;

	/**
	 * Method to be implemented by inheriting handlers
	 */
	public abstract void doCaseineCall() throws VplException, IOException;
	
	/**
	 * Call {@link doCaseineCall()} and returns false if a PasswordException was raised.
	 * Used to loop while given password is incorrect.
	 */
	public boolean tryCaseineCallForPassword() throws VplException, IOException {
		try {
			doCaseineCall();
			return true;
		} catch (PasswordException e) {
			return false;
		}
	}
	
	/**
	 * Find the project that is currently focused for this action, and check that it is a Caseine project.
	 * If not, set project to null.
	 * @param event The event parameter of the execute() method
	 */
	protected void inferAndCheckProjectFromEvent(ExecutionEvent event) {
		ISelection selectedObject = HandlerUtil.getCurrentSelection(event);
		project = WorkbenchUtils.getProjectFromSelection(selectedObject);

		if (project == null) {
			// No proper selection has been found in Package Explorer
			MessageDialog.openInformation(shell, "Caseine", "A project must be selected in Package Explorer.");
		} else {
			try {
				if(!project.hasNature(CaseineNature.NATURE_ID)){
					MessageDialog.openError(shell, "Error", "Not a Caseine Project !");
					project = null;
				}
			} catch (CoreException e){
				Activator.log(e);
				project = null;
			}
		}
	}
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		shell = HandlerUtil.getActiveWorkbenchWindowChecked(event).getShell();
		
		this.inferAndCheckProjectFromEvent(event);
		
		if (project != null) {
			moodleClient = WebserviceClientFactory.createFromProject(project);

			try {
				String password = null;
				while (!tryCaseineCallForPassword()) {
					// The VPL requires a password. Prompt for it and retry.
					PasswordDialog pwDialog = new PasswordDialog(shell);
					pwDialog.open();
					if (pwDialog.getReturnCode() == Window.OK) {
						password = pwDialog.getPassword();
						moodleClient.setPassword(password);
					} else {
						// The password dialog was cancelled - stop.
						password = null;
						break;
					}
				}
				if (password != null) {
					project.setPersistentProperty(PropertyConstants.VPLPASSWORD_PROPERTY, password);
				}
			} catch (VplException | IOException e) {
				String action;
				try {
					action = event.getCommand().getDescription();
				} catch (NotDefinedException e1) {
					action = "command execution";
				}
				if (e instanceof MaxFilesException) {
					MaxFilesListener.displayMaxFileDialog(shell, project, "Error",
							"Error during " + action + ":\n" + ErrorDisplayer.getDisplayableString(e), "OK");
				} else {
					ErrorDisplayer.displayAsDialog(e, shell, "Error during " + action);
				}
			} catch (CoreException e) {
				// This can be raised while setting the password property - just log it, the main process went well.
				Activator.log(e);
			}
		}
		
		return null;
	}

	/**
	 * Prompts user for confirmation to overwrite local files. Should only be called inside GUI thread.
	 * 
	 * @return {@code true} iff the user confirmed overwrite
	 */
	protected boolean openOverwriteDialog() {
		return MessageDialog.openConfirm(shell, "Confirmation",
				"Warning! You are about to overwrite your project at location :\n" +
						"\"" + project.getLocation().toOSString() + "\".\n" +
						"Are you sure you want to continue?");
	}

	/**
	 * Base source folder
	 */
	public static final String srcFolder = "src";
	/**
	 * Test folder (note : this is a source folder)
	 */
	public static final String testFolder = "test";
	/**
	 * Lib folder - contents will be added to classpath
	 */
	public static final String libFolder = "lib";
	/**
	 * All folders taken into account by this plugin for import and export
	 */
	public static final List<String> allFolders = Arrays.asList(srcFolder, testFolder, libFolder, "data");
	/**
	 * All special folders (this is equal to {@link #allFolders} without {@link #srcFolder})
	 */
	public static final List<String> specialFolders = allFolders.subList(1, 4);

	/**
	 * Pushes the local files to Caseine.
	 * 
	 * @param displayInfo - whether a dialog should be displayed in case of success.
	 *                    If {@code true}, should only be called inside GUI thread.
	 * @return {@code true} if the operation was successful, {@code false} if it has been canceled by the user.
	 */
	protected boolean doCaseineCallPush(boolean displayInfo) throws VplException, IOException {
		// Prompt to save any unsaved changes before push
		if (!PlatformUI.getWorkbench().saveAllEditors(true))
			return false;

		// Scan local project to list files
		ProjectType projectType = CaseineProjectWizard.getProjectType(moodleClient);
		String projectLocation = project.getLocation().toOSString();
		List<String> excludedFiles = FileUtils.scanExcludedList(projectLocation);
		List<VplFile> caseineFiles = new ArrayList<>();
		// Scan all folders for a Java project ; scan only root folder for other projects
		List<String> dirs = projectType == ProjectType.JAVA ? allFolders : Arrays.asList("");
		for (String dir : dirs) {
			String folder = projectLocation + (projectType == ProjectType.JAVA ? File.separator + dir : "");
			LOGGER.log(Level.INFO, "Scanning folder {0}...", folder);
			List<File> dirFiles = FileUtils.listIncludedFiles(folder, projectLocation, excludedFiles);
			for (File f : dirFiles) {
				// Cut src/ prefix for Java projects
				String fProjectRelativeName = f.getAbsolutePath()
						.replace(projectLocation + File.separator, "")
						.replace(File.separator, "/");
				if (fProjectRelativeName.startsWith(srcFolder + "/") && projectType == ProjectType.JAVA)
					fProjectRelativeName = fProjectRelativeName.substring(srcFolder.length() + 1);

				caseineFiles.add(new VplFile(f, fProjectRelativeName));
			}
		}

		// Export
		moodleClient.callServiceWithFiles("mod_vpl_save", caseineFiles);

		if (displayInfo) {
			MessageDialog.openInformation(shell, "Caseine", "Caseine Lab successfully exported!");
		}

		return true;
	}

	/**
	 * Import files from the VPL on Caseine
	 * 
	 * @param reset - whether this method should import the initial files
	 *              (if {@code true}) or the current submission (if {@code false})
	 */
	protected void doCaseineCallPull(boolean reset) throws VplConnectionException, MoodleWebServiceException, IOException {
		doCaseineCallPull(shell, project, moodleClient, reset, false, reset ? "reset" : "imported");
	}

	/**
	 * Import files from the VPL on Caseine
	 * 
	 * @param shell
	 * @param project
	 * @param moodleClient
	 * @param initial      - whether this method should import the initial files (if
	 *                     {@code true}) or the current submission (if
	 *                     {@code false})
	 * @param includeGILib - whether the GI libraries should be included in the
	 *                     project
	 * @param message      - the action to display in case of success (eg.:
	 *                     "reset","imported","created")
	 */
	public static void doCaseineCallPull(Shell shell, IProject project, RestJsonMoodleClient moodleClient,
			boolean initial, boolean includeGILib, String message)
			throws VplConnectionException, MoodleWebServiceException, IOException {

		ProjectType projectType = CaseineProjectWizard.getProjectType(moodleClient);
		String projectDir = project.getLocation().toOSString();

		if (initial) {
			MaxFilesListener.generateVplignore(projectDir);
		}

		// Delete previous files
		List<String> excludedFiles = FileUtils.scanExcludedList(projectDir);
		for (String folder : allFolders) {
			FileUtils.deleteIncludedFolder(new File(projectDir + File.separator + folder), projectDir, excludedFiles);
		}

		String srcDir = projectDir + (projectType == ProjectType.JAVA ? File.separator + srcFolder : "");
		if (projectType == ProjectType.JAVA) {
			// Always create src as it is referenced in classpath
			new File(srcDir).mkdirs();
		}

		VplFile[] files = initial ?
						RestJsonMoodleClient.extractFiles(moodleClient.callService("mod_vpl_info"), "reqfiles") :
						RestJsonMoodleClient.extractFiles(moodleClient.callService("mod_vpl_open"), "files");

		List<String> sourceEntries = new ArrayList<>();
		List<String> libEntries = new ArrayList<>();

		for (int i = 0; i < files.length; i++) {
			String name = files[i].getFullOSName();
			String path = files[i].getOSPath();
			int sizeOfSuperDirName = path.indexOf(File.separator);
			String folder = sizeOfSuperDirName > 0 ? path.substring(0, sizeOfSuperDirName) : path;

			String parentDir;

			if (specialFolders.contains(folder) && projectType == ProjectType.JAVA) {
				// The file is in a special folder : put it at the root of the project
				parentDir = projectDir;
				if (folder.equals(testFolder) && !sourceEntries.contains(testFolder)) {
					// There is at least a file in the test folder - add it as a source folder
					sourceEntries.add(testFolder);
				} else if (folder.equals(libFolder) && name.endsWith(".jar")) {
					// JAR files in the lib folder are to be treated as lib entries
					libEntries.add(name);
				}
			}
			else {
				// The file isn't in a special folder : put it in src
				parentDir = srcDir;
			}
			
			// Create any missing directories
			File dir = new File(parentDir + File.separator + path);
			if (!dir.exists() && !dir.mkdirs()) {
				MessageDialog.openError(shell, "Error", "Problem with directory creation: " + dir);
				continue;
			}
			
			// Don't overwrite vplignored files
			String fProjectRelativeName = (parentDir + "/" + name)
					.replace(projectDir + File.separator, "")
					.replace(File.separator, "/");
			if (!FileUtils.isExcluded(fProjectRelativeName, excludedFiles)) {
				// Write the file on disk
				FileOutputStream fos = new FileOutputStream(parentDir + File.separator + name);
				files[i].write(fos);
				fos.close();
			}
		}
		
		try {
			if (projectType == ProjectType.JAVA) {
				setupJavaProjectClasspath(project, libEntries, sourceEntries, includeGILib);
			}
			project.refreshLocal(IResource.DEPTH_INFINITE, null);
		} catch (CoreException e) {
			Activator.log(e);
			return;
		}

		MessageDialog.openInformation(shell, "Caseine", "Caseine Lab successfully " + message + "!");
	}
	
	protected static void setupJavaProjectClasspath(IProject project, List<String> libEntries, List<String> sourceEntries, boolean includeGILib) throws CoreException, IOException {
		ArrayList<IClasspathEntry> newEntries = new ArrayList<>();
		for (String libEntry : libEntries) {
			newEntries.add(JavaCore.newLibraryEntry(project.getFullPath().append(libEntry),null,null));
		}
		for (String sourceEntry : sourceEntries) {
			newEntries.add(JavaCore.newSourceEntry(project.getFullPath().append(sourceEntry)));
		}
		
		// Apply the new classpath entries to project classpath
		IJavaProject javaProject = JavaCore.create(project);
		ArrayList<IClasspathEntry> entries = new ArrayList<>(Arrays.asList(javaProject.getRawClasspath()));
		for (IClasspathEntry newEntry : newEntries) {
			if (!entries.contains(newEntry)) {
				entries.add(newEntry);
			}
		}
		javaProject.setRawClasspath(entries.toArray(new IClasspathEntry[0]), true, null);
		
		if (includeGILib) {
			AddGILibrariesHandler.addGILibraries(javaProject);
		}
	}
}
