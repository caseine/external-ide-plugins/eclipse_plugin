/*
 * Copyright 2018, 2019: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Universit� Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package caseine.vpl.handlers;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import javax.json.JsonObject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PartInitException;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import caseine.vpl.Activator;
import caseine.vpl.ErrorDisplayer;
import caseine.vpl.preferences.PreferenceConstants;
import caseine.vpl.views.vplview.VplView;
import vplwsclient.RestJsonMoodleClient;
import vplwsclient.exception.MoodleWebServiceException;
import vplwsclient.exception.VplConnectionException;
import vplwsclient.exception.VplException;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * 
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 * @author Christophe Saint-Marcel
 */
public class EvaluateProjectHandler extends CaseineHandler {
	
	public void doCaseineCall() throws VplException, IOException {
		if (doCaseineCallPush(false)) {
			try {
				VplView view = VplView.findAndShow();
				(new EvaluationJob("Evaluation is in progress...", this.moodleClient, view)).schedule();
			} catch (PartInitException e) {
				// The workspace is in a peculiar state, write the error to the workspace log.
				Activator.log(e);
			}
		}
	}
	
	class EvaluationJob extends Job {
		/**
		 * JsonObject containing the result of the evaluation
		 */
		protected JsonObject result;
		
		protected IProgressMonitor monitor;

		private VplView view;
		
		private RestJsonMoodleClient moodleClient;
		
		private static final String GRADE = "grade";
		private static final String COMPILATION = "compilation";
		private static final String EVALUATION = "evaluation";
		
		public EvaluationJob(String name, RestJsonMoodleClient moodleClient, VplView view) {
			super(name);
			this.moodleClient = moodleClient;
			this.view = view;
		}
		
		@Override
		protected IStatus run(IProgressMonitor monitor) {
			IStatus status = Status.OK_STATUS;
			
			this.monitor = monitor;
			monitor.beginTask("Evaluation...", IProgressMonitor.UNKNOWN);

			try {
				// Create a GUI delegate thread (required to make view modifications)
				Display.getDefault().syncExec(() -> {
					this.view.clearResult();
					this.view.showResultTab();
					this.view.setGrade("Evaluation is in progress...");
				});
				this.evaluateAndRetrieveResult();
				this.showTestView(false);
			}
			catch (VplConnectionException | MoodleWebServiceException e){
				this.showTestView(true);
				// Create a GUI delegate thread (necessary to display a dialog)
				Activator.log(e);
				Display.getDefault().syncExec(() -> ErrorDisplayer.displayAsDialog(e, shell, "Error during project evaluation"));
			}
			catch (InterruptedException e) {
				this.showTestView(true);
				status = Status.CANCEL_STATUS;
				Thread.currentThread().interrupt();
			}
			
			monitor.done();
			return status;
		}
		
		/**
		 * Show the view of result
		 * @param clear - whether the view should be cleared of any display
		 */
		protected void showTestView(boolean clear) {
			Display.getDefault().syncExec(() -> {
				// Create a GUI delegate thread (required to make view modifications)
				this.view.synchronize();
				this.view.showResultTab();
				if (!clear) {
		    		this.view.setGrade(result.getString(GRADE, "No grade could be retrieved."));
		    		this.view.setResult(result.getString(EVALUATION, ""), result.getString(COMPILATION, ""), (new SimpleDateFormat()).format(new Date()));
				}
		    	else{
		    		this.view.clearResult();
				}
			});
		}
		
		/**
		 * Evaluate and get the result of evaluation and store it into {@link #result}
		 */
		protected void evaluateAndRetrieveResult() throws VplConnectionException, MoodleWebServiceException, InterruptedException {
			// Call mod_vpl_evaluate.
			JsonObject monitorUrls = this.moodleClient.callService("mod_vpl_evaluate");
			
			IPreferenceStore store = Activator.getDefault().getPreferenceStore();
			if (store.getString(PreferenceConstants.P_EVALUATION_METHOD).equals(PreferenceConstants.EVALUATION_METHOD_LISTENWS)) {
				for (String field : Arrays.asList("smonitorURL", "monitorURL")) {
					String monitorUrl = monitorUrls.getString(field, "");
					if (monitorUrl.length() > 0) {
						// We have a monitor URL, listen to it then retrieve result.
						this.listenToWebSocket(monitorUrl);
						this.retrieveResult();
						return;
					}
				}
				// Preferences were set to listen to websocket, but no url was returned by the server.
				Activator.log(Status.WARNING, "No monitor URL specified by the \"evaluate\" webservice. Falling back to compatibility mode.");
			}
			
			int waitTime = 5;
			boolean retryEvaluate;
			do {
				retryEvaluate = false;
				this.waitWithProgress(waitTime, "Waiting for server to finish execution");

				this.retrieveResult();

				String jailevaluationNotCompleteMessage = "The compilation process did not generate an executable nor error message.";

				if (this.result.getString(EVALUATION,"").equals("")
						&& this.result.getString(COMPILATION,"").equals("")
						|| this.result.getString(COMPILATION,"").equals(jailevaluationNotCompleteMessage)) {

					String message;
					if (this.result.getString(COMPILATION,"").equals(jailevaluationNotCompleteMessage)) {
						message = jailevaluationNotCompleteMessage;
					} else {
						message = "Failed to retrieve evaluation result.";
					}
					message += " The server may be busy, or execution especially long. Retry with greater delay?";
					
					retryEvaluate = this.promptForRetry("Service error", message);
					
					if (retryEvaluate) {
						this.moodleClient.callService("mod_vpl_evaluate");
						waitTime += 5;
					}
				}
			} while (retryEvaluate);
		}
		
		/**
		 * Listen to the given url via websocket during evaluation.
		 * @param monitorUrl URL to listen to.
		 * @throws VplConnectionException If anything goes wrong or unexpected when listening to execution server.
		 */
		private void listenToWebSocket(String monitorUrl) throws VplConnectionException, InterruptedException {
			try {
				class EvaluationClient extends WebSocketClient {
					public EvaluationClient(URI serverUri) {
						super(serverUri);
					}
					private boolean retrieveFlagRecieved = false;
					private String lastMessage = "";
					private String lastRawMessage = "";
					private String wsError = "";
					@Override
					public void onMessage(String rawMessage) {
						lastRawMessage = rawMessage.trim();
			    		int indexOfMessage = rawMessage.indexOf("message:");
			    		if (indexOfMessage > -1) {
			    			// Display the status message on the monitor.
			    			String message = rawMessage.substring(indexOfMessage + 8);
			    			if (message.length() > 0) {
			    				message = Character.toUpperCase(message.charAt(0)) + message.substring(1);
			    			}
			    			monitor.setTaskName(message);
			    			lastMessage = message;
			    		}
			    		if (rawMessage.endsWith("retrieve:")) {
				    		// "retrieve:" marker recieved: evaluation is finished, stop execution.
			    			retrieveFlagRecieved = true;
							this.close();
				    	}
					}
					@Override
					public void onOpen(ServerHandshake handshake) {
						// Nothing to init here.
					}
					@Override
					public void onClose(int code, String reason, boolean remote) {
						// Not necessarily an error, but store it as an error if needed.
						wsError = reason.length() > 0 ? "[" + code + "] " + reason : "code " + code;
					}
					@Override
					public void onError(Exception e) {
						wsError = e.getLocalizedMessage();
					}
					public String generateDetailedErrorMessage() {
						String errorMsg = "Unexpected end of communication with execution server";
						if (wsError.length() > 0) {
						    errorMsg += " (" + wsError + ")";
					    }
						errorMsg += ".";
					    if (lastMessage.length() > 0) {
						    errorMsg += "\nLast message recieved: " + lastMessage;
					    }
					    if (lastRawMessage.length() > 0) {
						    errorMsg += "\nLast bytes recieved: " + lastRawMessage;
					    }
					    return errorMsg;
					}
				}

				EvaluationClient client = new EvaluationClient(new URI(monitorUrl));
				
				// Open websocket.
				client.connectBlocking();
				
				// Wait for the socket to be closed (either by remote or by us when the retrieve: flag is recieved).
				while (client.isOpen()) {
					Thread.onSpinWait();
				}
				
				// Ensure the connection is closed before retrieving the result.
				client.closeBlocking();
				
				if (!client.retrieveFlagRecieved) {
				    throw new VplConnectionException(new IOException(client.generateDetailedErrorMessage()));
				}
			} catch (URISyntaxException e) {
				throw new VplConnectionException(e);
			}
		}
		
		/**
		 * Wait for the given time and notify the monitor with progress.
		 * @param waitTime Wait time in seconds
		 * @param message
		 */
		private void waitWithProgress(int waitTime, String message) throws InterruptedException {
			for (int deciseconds = 0; deciseconds < waitTime * 10; deciseconds ++) {
				this.monitor.setTaskName(message + " (" + Math.round(deciseconds * 10.0 / waitTime) + "%)...");
				Thread.sleep(100);
			}
		}
		
		/**
		 * Call get_result webservice.
		 */
		private void retrieveResult() throws VplConnectionException, MoodleWebServiceException {
			this.monitor.setTaskName("Retrieving result...");
			this.result = this.moodleClient.callService("mod_vpl_get_result");
		}
		
		/**
		 * Shows a prompt to the user with two options, "Retry" or "Cancel".
		 * @param title Message box title
		 * @param prompt Message to display
		 * @return true if retry was requested.
		 */
		private boolean promptForRetry(String title, String prompt) {
			class RetryPrompter implements Runnable {
				private int userAnswer;
				public void run() {
					// Create a GUI delegate thread (necessary to display a dialog)
					MessageDialog retryDialog = new MessageDialog(shell, title, null,
							prompt,
							MessageDialog.ERROR, new String[] { "Retry","Cancel" }, 0);
					
					userAnswer = retryDialog.open();
				}
			}
			
			RetryPrompter retryPrompter = new RetryPrompter();
			Display.getDefault().syncExec(retryPrompter);
			return (retryPrompter.userAnswer == 0);
		}
		
	// End : EvaluationJob
	}
}
