/*
 * Copyright 2018, 2019: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package caseine.vpl.decorators;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.DecorationOverlayIcon;
import org.eclipse.jface.viewers.IDecoration;
import org.eclipse.jface.viewers.ILabelDecorator;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

import caseine.vpl.Activator;
import caseine.vpl.nature.CaseineNature;

/**
 * A decorator to set the project icon.
 * @author Christophe Saint-Marcel
 * @author Astor Bizard
 */
public class ProjectDecorator extends LabelProvider implements ILabelDecorator {

	@Override
	public Image decorateImage(Image baseImage, Object element) {
		IProject project = (IProject) element;
		try {
			if (project.isAccessible() && project.isNatureEnabled(CaseineNature.NATURE_ID)) {
				// Image for caseine project
				ImageDescriptor laitImage = Activator.getImageDescriptor("icons/laitcaseine.png");
				
				// Icon for project compilation status (error/warning)
				ImageDescriptor statusIcon = null;
				int severity = project.findMaxProblemSeverity(null, true, IResource.DEPTH_INFINITE);
				if(severity == IMarker.SEVERITY_ERROR)
					statusIcon = PlatformUI.getWorkbench().getSharedImages().getImageDescriptor(ISharedImages.IMG_DEC_FIELD_ERROR);
				else if(severity == IMarker.SEVERITY_WARNING)
					statusIcon = PlatformUI.getWorkbench().getSharedImages().getImageDescriptor(ISharedImages.IMG_DEC_FIELD_WARNING);
				
				// Combine image and icon
				return new DecorationOverlayIcon(laitImage.createImage(), statusIcon, IDecoration.BOTTOM_LEFT).createImage();
			}
		} catch (CoreException e) {
			Activator.log(e);
		}

		return baseImage;
	}

	@Override
	public String decorateText(String arg0, Object arg1) {
		return null;
	}

}