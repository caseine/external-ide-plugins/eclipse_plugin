/*
 * Copyright 2018, 2019: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package caseine.vpl.preferences;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.RadioGroupFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import caseine.vpl.Activator;

/**
 * This class represents a preference page that
 * is contributed to the Preferences dialog. By 
 * subclassing <samp>FieldEditorPreferencePage</samp>, we
 * can use the field support built into JFace that allows
 * us to create a page that is small and knows how to 
 * save, restore and apply itself.
 * <p>
 * This page is used to modify preferences only. They
 * are stored in the preference store that belongs to
 * the main plug-in class. That way, preferences can
 * be accessed directly via the preference store.
 * @author Christophe Saint-Marcel
 */

public class CaseinePreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

	public CaseinePreferencePage() {
		super(FLAT);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
	}
	
	/**
	 * Text field for custom URL
	 */
	private StringFieldEditor customUrlField;
	
	/**
	 * Creates the field editors. Field editors are abstractions of
	 * the common GUI blocks needed to manipulate various types
	 * of preferences. Each field editor knows how to save and
	 * restore itself.
	 */
	public void createFieldEditors() {
		
		Composite parent = getFieldEditorParent();
		
		Group urlGroup = new Group(parent,SWT.FILL);
		urlGroup.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 2, 1));
		urlGroup.setText("VPL Webservice URL:");
		
		// Radio buttons for URL choice
		RadioGroupFieldEditor urlChoiceField = new RadioGroupFieldEditor(PreferenceConstants.P_URL_CHOICE, "", 1,
				new String[][] {{ "&Caseine", PreferenceConstants.URL_CHOICE_CASEINE },
				                { "Custo&m:", PreferenceConstants.URL_CHOICE_CUSTOM }}, urlGroup) {
			@Override
			protected void fireValueChanged(String property, Object oldValue, Object newValue) {
				super.fireValueChanged(property, oldValue, newValue);
				// Selection on radio buttons changed : enable/disable the custom URL text field
				if (PreferenceConstants.URL_CHOICE_CUSTOM.equals(newValue)) {
					customUrlField.setEnabled(true, urlGroup);
					customUrlField.setFocus();
					checkUrl(customUrlField.getTextControl(urlGroup).getText());
				}
				else{
					customUrlField.setEnabled(false, urlGroup);
					updateStatus(null);
				}
			}
			
			@Override
		 	protected void doStore(){
		 		super.doStore();
		 		// The choice on radio buttons is automatically stored, but we have to store the P_URL preference manually
		 		// as it is not linked to the buttons
		 		Control[] radioButtons = this.getRadioBoxControl(urlGroup).getChildren();
		 		for(int i=0; i<radioButtons.length; i++){
		 			Button b = (Button)radioButtons[i];
		 			if(b.getSelection()){
		 				String url;
		 				if(PreferenceConstants.URL_CHOICE_CASEINE.equals(b.getData()))
		 					url = PreferenceConstants.URL_CASEINE;
		 				else
		 					url = customUrlField.getStringValue();
		 				getPreferenceStore().setValue(PreferenceConstants.P_URL,url);
		 				break;
		 			}
		 		}
		 	}
		};
		GridLayout radioButtonsLayout = new GridLayout();
		radioButtonsLayout.marginTop = 3;
		urlChoiceField.getRadioBoxControl(urlGroup).setLayout(radioButtonsLayout);
		urlChoiceField.getRadioBoxControl(urlGroup).getChildren()[0].setToolTipText(PreferenceConstants.URL_CASEINE);
		addField(urlChoiceField);
		
		customUrlField = new StringFieldEditor(PreferenceConstants.P_CUSTOM_URL, "", urlGroup){
			@Override
			protected void fireValueChanged(String property, Object oldValue, Object newValue) {
				super.fireValueChanged(property, oldValue, newValue);
				checkUrl((String)newValue);
			}
		};
		customUrlField.setEnabled(getPreferenceStore().getString(PreferenceConstants.P_URL_CHOICE).equals(PreferenceConstants.URL_CHOICE_CUSTOM), urlGroup);
		addField(customUrlField);
		
		// Text field for token
		StringFieldEditor tokenField = new StringFieldEditor(PreferenceConstants.P_SECURITY_KEY, "User Security &Key (Token):", parent);
		// Hack to set text field as password while keeping FieldEditor convenience
		Text dummyPassword = new Text(parent, SWT.PASSWORD);
		tokenField.getTextControl(parent).setEchoChar(dummyPassword.getEchoChar());
		dummyPassword.dispose();
		addField(tokenField);
		
		// Radio buttons for evaluation method choice
		RadioGroupFieldEditor evaluationMethodChoiceField = new RadioGroupFieldEditor(PreferenceConstants.P_EVALUATION_METHOD, "Evaluation method:", 1,
				new String[][] {{ "&Listen to execution server via websocket (recommended)", PreferenceConstants.EVALUATION_METHOD_LISTENWS },
								{ "&Wait for a few seconds (compatibility mode)", PreferenceConstants.EVALUATION_METHOD_WAIT }}, parent);
		evaluationMethodChoiceField.getRadioBoxControl(parent).setLayout(new GridLayout());
		evaluationMethodChoiceField.getRadioBoxControl(parent).getChildren()[0].setToolTipText(
				"Listen to execution server via websockets, to know when evaluation is over.\n"
				+ "This method is faster but may fail on some systems.");
		evaluationMethodChoiceField.getRadioBoxControl(parent).getChildren()[1].setToolTipText(
				"Wait for an arbitrary 5 seconds for evaluation to finish before retrieving result,\n"
				+ "repeating and extending wait time if needed.\n"
				+ "Choose this method if the other one raises errors.");
		addField(evaluationMethodChoiceField);
	
	}
	
	private void checkUrl(String url){
		boolean isValidUrl = url.startsWith("http://") || url.startsWith("https://");
		updateStatus(isValidUrl ? null : "Invalid URL: should be either http or https.");
	}
	
	private void updateStatus(String message){
		setMessage(message,ERROR); // using setErrorMessage() instead of this leads to strange display bugs
		setValid(message == null);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench) {
		// Nothing to init.
	}
	
}