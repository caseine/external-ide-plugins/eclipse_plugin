/*
 * Copyright 2018, 2019: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Universit� Grenoble Alpes
 * Institut Polytechnique de Grenoble
 *
 *
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package caseine.vpl.views.vplview;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonValue;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import caseine.vpl.Activator;
import caseine.vpl.ErrorDisplayer;
import caseine.vpl.WebserviceClientFactory;
import caseine.vpl.WorkbenchUtils;
import caseine.vpl.nature.CaseineNature;
import vplwsclient.RestJsonMoodleClient;
import vplwsclient.exception.MoodleWebServiceException;
import vplwsclient.exception.VplConnectionException;

public class VplView extends ViewPart {

    public static final String VIEW_ID = "caseine.vpl.vplview";

    private Composite parent;

    private TabFolder tabFolder;
    private TabItem descriptionTab;
    private TabItem resultTab;
    private Label projectNameLabel;
    private Label clockLabel;
    private Label evaluationsLabel;

    private Composite subrestrictionsView;
    private GridData subRestrictionsData;
    private SubmissionRestrictions subrestrictions;

    private Browser browser;
    private ResultTreeView resultTree;

    private IProject project;

    public static VplView findAndShow() throws PartInitException {
        return (VplView) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(VIEW_ID);
    }

    private ISelectionListener listener = new ISelectionListener() {
        public void selectionChanged(IWorkbenchPart sourcepart, ISelection selection) {
            // Do not update the view if it is the same project (so it doesn't erase the
            // evaluation result)
            IProject previousProject = project;
            project = WorkbenchUtils.getProjectFromSelection(selection);
            if (project == previousProject)
                return;
            if (project == null) {
                project = previousProject;
                return;
            }

            recoverAndDisplayInfos();
        }
    };

    @Override
    public void createPartControl(Composite parent) {
        this.parent = parent;
        GridLayout monoColumn = new GridLayout(1, false);

        parent.setLayout(monoColumn);

        // Vpl name
        projectNameLabel = new Label(parent, SWT.WRAP);
        projectNameLabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

        // container for clock and remaining evaluations
        subrestrictionsView = new Composite(parent, SWT.NONE);
        subRestrictionsData = new GridData(SWT.FILL, SWT.FILL, true, false);
        subrestrictionsView.setLayoutData(subRestrictionsData);
        subrestrictionsView.setLayout(new GridLayout(2, true));
        GridData clockAndEvalsData = new GridData(GridData.FILL, GridData.FILL, true, false);

        // Clock
        clockLabel = new Label(subrestrictionsView, SWT.CENTER | SWT.WRAP);
        clockLabel.setLayoutData(clockAndEvalsData);

        // Remaining evaluations
        evaluationsLabel = new Label(subrestrictionsView, SWT.CENTER | SWT.WRAP);
        evaluationsLabel.setLayoutData(clockAndEvalsData);
        subrestrictions = new SubmissionRestrictions(this, clockLabel, evaluationsLabel);

        // Two tabs : description and results
        tabFolder = new TabFolder(parent, SWT.NONE);
        tabFolder.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

        descriptionTab = new TabItem(tabFolder, SWT.NONE);
        descriptionTab.setText("Description");
        Composite descriptionTabPage = new Composite(tabFolder, SWT.NONE);
        descriptionTabPage.setLayout(monoColumn);
        browser = new Browser(descriptionTabPage, SWT.NONE);
        browser.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        browser.setBounds(0, 0, 600, 400);
        Link resetDescrButton = new Link(descriptionTabPage, SWT.NONE);
        resetDescrButton.setLayoutData(new GridData(SWT.END, SWT.FILL, true, false));
        resetDescrButton.setText("<a>Reload description</a>");
        resetDescrButton.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                recoverAndDisplayInfos();
            }
        });
        descriptionTab.setControl(descriptionTabPage);

        resultTab = new TabItem(tabFolder, SWT.NONE);
        resultTab.setText("Result");
        Composite resultTabPage = new Composite(tabFolder, SWT.NONE);
        resultTabPage.setLayout(monoColumn);
        resultTree = new ResultTreeView(resultTabPage, SWT.NONE);
        resultTree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        resultTab.setControl(resultTabPage);

        parent.pack();

        getSite().getPage().addSelectionListener(listener);
    }

    public void dispose() {
        getSite().getPage().removeSelectionListener(listener);
        tabFolder.dispose();
        projectNameLabel.dispose();
        super.dispose();
    }

    @Override
    public void setFocus() {
        this.tabFolder.setFocus();
    }

    /**
     * Synchronize the clock and the number of remaining free evaluations.
     * Called every 5 minutes plus on several actions (chaging project, call
     * evaluation).
     */
    public void synchronize() {
        try {
            if (project == null || !project.exists() || !project.hasNature(CaseineNature.NATURE_ID)) {
                subRestrictionsData.exclude = true;
                subrestrictionsView.setVisible(false);
                return;
            }
            if (subrestrictions.synchronize(project)) {
                subRestrictionsData.exclude = false;
                subrestrictionsView.setVisible(true);
            } else {
                subRestrictionsData.exclude = true;
                subrestrictionsView.setVisible(false);
            }
        } catch (CoreException e) {
            Activator.log(e);
            return;
        }
    }

    /**
     * Function called every second to update the clock.
     */
    public void updateClock() {
        if (project == null)
            return;
        try {
            if (project == null || !project.exists()) {
                clockLabel.setText("");
                clockLabel.setToolTipText("");
                return;
            }
            if (!project.hasNature(CaseineNature.NATURE_ID)) {
                clockLabel.setText("");
                clockLabel.setToolTipText("");
                return;
            }
            subrestrictions.updateClockLabel();
        } catch (CoreException e) {
            Activator.log(e);
            return;
        }
    }

    /**
     * Deprecated, manually increment the number of used evaluations (reduce the
     * number of free evaluations)
     */
    public void incrementUsedEvaluations() {
        subrestrictions.incrementUsedEvaluations();
    }

    public void setResult(String result, String compilation, String date) {
        resultTree.setResult(result, compilation, date);
    }

    public void setGrade(String grade) {
        resultTree.setGrade(grade);
    }

    public void setDescription(String title, String intro) {
        this.projectNameLabel.setText(title);
        this.browser.setText(intro);
        parent.layout();
    }

    public void recoverAndDisplayInfos() {
        try {
            this.synchronize();
            if (project == null || !project.exists())
                return;
            if (project.hasNature(CaseineNature.NATURE_ID))
                recoverAndDisplayInfos(WebserviceClientFactory.createFromProject(project));
            else {
                setDescription("", "Not a Caseine project");
                setGrade("Not a Caseine project");
                setResult("", "", "");
            }
        } catch (CoreException e) {
            Activator.log(e);
        }
    }

    public void recoverAndDisplayInfos(RestJsonMoodleClient moodleClient) {
        this.showDescriptionTab();

        // Recover and display description
        try {
            JsonObject intro = moodleClient.callService("mod_vpl_info");
            String introTitle = intro.getString("name", "");
            String introHtml = intro.getString("intro", "A problem occured while recovering the VPL description.");
            this.setDescription(introTitle, introHtml);
        } catch (MoodleWebServiceException | VplConnectionException e) {
            this.setDescription("", ErrorDisplayer.getDisplayableString(e).replace("\n", "<br>"));
        }

        // Recover and display last evaluation
        try {
            JsonObject result;
            result = moodleClient.callService("mod_vpl_get_last_evaluation");
            setGrade(result.getString("grade", "Not yet evaluated..."));
            JsonValue jsonDate = result.get("timesubmitted");
            String date;
            if (jsonDate.getValueType() == JsonValue.ValueType.NUMBER) {
                JsonNumber jsonNumberDate = result.getJsonNumber("timesubmitted");
                if (jsonNumberDate == null)
                    date = "Unknown date";
                if (jsonNumberDate.longValue() == 0)
                    date = "Ancient evaluation";
                else
                    date = (new SimpleDateFormat()).format(new Date(jsonNumberDate.longValue() * 1000));
            } else 
                date = "Unknown date";
            setResult(result.getString("evaluation", ""), result.getString("compilation", ""), date);
        } catch (MoodleWebServiceException | VplConnectionException e) {
            this.setGrade("");
            this.setResult("", "", "");
        }
    }

    public void showResultTab() {
        this.tabFolder.setSelection(this.resultTab);
    }

    public void showDescriptionTab() {
        this.tabFolder.setSelection(this.descriptionTab);
    }

    public void clearResult() {
        resultTree.clear();
        setGrade("Not yet evaluated...");
    }
}
