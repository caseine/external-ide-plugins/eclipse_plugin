/*
 * Copyright 2018, 2019: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Universit� Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package caseine.vpl.views.vplview;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import org.eclipse.jface.viewers.DelegatingStyledCellLabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Device;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Tree;

import caseine.vpl.Activator;
class ResultTreeView extends Composite {
	
	// Our JUnit view conventions
	static final String TestsRootName = "All tests";
	static final String CompilationErrorsName = "Compilation errors";

	// Evaluation result parsing setup
	static final String VplTestsHeader = "-Tests results";
	static final String VplTestId = "-";
	static final String VplCompilationErrorsId = "-The compilation"; // ...or preparation of compilation has failed
	static final String VplIncorrectTestId = "Incorrect"; // ...program result
	static final String VplTestDetailsId = "Message";

	private TreeViewer viewer;
    private Tree t;
    private TCP tcp;
    private TLP tlp;
    private StyledText gradeText;
    private StyledText failureDetails;
    private Label testsLabel;
    private Label errorsLabel;
    private Label failuresLabel;
    private Label colorRectangle;
    private Color redHeaderColor;
    private Color greenHeaderColor;
    
    class TCP implements ITreeContentProvider {

    	private TestResult testsRoot;
    	private ArrayList<TestResult> tests;
    	private TestResult errors;

    	TCP(){
    		tests = new ArrayList<>();
    		testsRoot = new TestResult(null,"");
    		errors = null;
    	}
    	
    	Object getInitial(){
    		return t;
    	}
    	
    	@Override
		public Object[] getChildren(Object element) {
			return ((TestResult)element).getChildren();
		}

		@Override
		public Object[] getElements(Object element) {
			if (element == t) {
				// The tree root element.
				return errors==null ? new Object[]{testsRoot} : new Object[]{errors,testsRoot};
			} else {
				return getChildren(element);
			}
		}

		@Override
		public Object getParent(Object element) {
			return ((TestResult)element).getParent();
		}

		@Override
		public boolean hasChildren(Object element) {
			return ((TestResult)element).hasChildren();
		}

		void setContent(String result, String compilation, String date) {
			tests.clear();
			
			boolean doClear = (result.length()==0);
			
			if(doClear){
				testsRoot = new TestResult(null,"");
			}
			else{
				testsRoot = new TestResult(null,TestsRootName + " [" + date + "]");
				
				BufferedReader br = new BufferedReader(new StringReader(result));
				String line;
				StringBuilder currentTest = new StringBuilder();
				TestResult group = testsRoot;
				try {
					while((line=br.readLine()) != null){
						if(line.startsWith(VplTestsHeader))
							// Header line : skip
							continue;
						else if(line.startsWith(VplTestId)){
							// First line of a test
							line = line.substring(VplTestId.length()).trim();
							
							if(line.matches(".*\\([0-9]+/[0-9]+\\)")){
								// Remove the test number
								line = line.substring(0,line.lastIndexOf('(')).trim();
							}
							if(currentTest.length() > 0){
								// Add the previous test to the list
								tests.add(new TestResult(group,currentTest.toString()));
							}
							currentTest = new StringBuilder();
							
							int groupNameLength = line.lastIndexOf(".");
							if(groupNameLength>0){
								String groupName = line.substring(0,groupNameLength);
								if(group==null || !group.getName().equals(groupName))
									group = new TestResult(testsRoot,groupName);
								
								line = line.substring(groupNameLength+1);
							}
							
						}
						currentTest.append(line + "\n");
					}
					br.close();
				}
				catch (IOException e) {
					// Something went wrong while reading input - keep proceeding with partial data.
				}
				// Add the last test to the list
				tests.add(new TestResult(group,currentTest.toString()));

				if(testsRoot.isOk())
					colorRectangle.setBackground(greenHeaderColor);
				else
					colorRectangle.setBackground(redHeaderColor);
				
				testsLabel.setText(tests.size() + " Tests");
				int nbFailures = 0;
				int nbErrors = 0;
				for(TestResult tr : tests)
					if(tr.isFail())
						nbFailures++;
					else if(tr.isError())
						nbErrors++;
				errorsLabel.setText("Errors : " + nbErrors);
				failuresLabel.setText("Failures : " + nbFailures);
			}

			colorRectangle.setVisible(!doClear);
			setLabelsVisible(!doClear);
			
			if(compilation.length()==0)
				errors = null;
			else{
				errors = new TestResult(null,CompilationErrorsName + "\n" + compilation);
			}
		}
    }
    
    class TLP extends DelegatingStyledCellLabelProvider{
		
		TLP(){
			super(new IStyledLabelProvider() {

				@Override
				public void addListener(ILabelProviderListener arg0) { /* This is not needed with this usage */ }
				@Override
				public void removeListener(ILabelProviderListener arg0) { /* This is not needed with this usage */ }
				@Override
				public void dispose() { /* Nothing to dispose */ }
				@Override
				public boolean isLabelProperty(Object arg0, String arg1) {  /* This is not needed with this usage */ return false; }
				
				@Override
				public Image getImage(Object element) {
					TestResult tr = (TestResult)element;
					if(tr.getName().length()==0)
						return null;
					String iconName = "icons/JUnit_";
					if(tcp.hasChildren(element))
						iconName += "Suite";
					else{
						iconName += "Case";
					}
					iconName += (tr.isOk() ? "Ok" : (tr.isFail() ? "Fail" : "Error")) + ".png";
					return Activator.getImageDescriptor(iconName).createImage();
				}
				
				@Override
				public StyledString getStyledText(Object element) {
					String text = ((TestResult)element).getName();
					StyledString res = new StyledString(text);
					if(text.startsWith(TestsRootName)){
						// Put the date in another color
						res.setStyle(TestsRootName.length(), res.length()-TestsRootName.length(), StyledString.DECORATIONS_STYLER);
					}
					return res;
				}
		    	
			});
		}
		
    }
    
    ResultTreeView(Composite parent, int style){
    	super(parent,style);

    	this.setLayout(new GridLayout(3,true));
    	
		GridData gdLabels = new GridData(GridData.FILL,GridData.FILL,true,false);
		
		testsLabel = new Label(this, SWT.CENTER);
		testsLabel.setLayoutData(gdLabels);
		errorsLabel = new Label(this, SWT.CENTER);
		errorsLabel.setLayoutData(gdLabels);
		failuresLabel = new Label(this, SWT.CENTER);
		failuresLabel.setLayoutData(gdLabels);
		setLabelsVisible(false);

		gradeText = new StyledText(this, SWT.BORDER);
		gradeText.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,false,3,1));
		gradeText.setEditable(false);
		gradeText.setEnabled(false);
		
		colorRectangle = new Label(this, SWT.NONE);
		GridData gdRectangle = new GridData(SWT.FILL,SWT.FILL,true,false,3,1);
		gdRectangle.heightHint = 20;
		colorRectangle.setLayoutData(gdRectangle);
		colorRectangle.setVisible(false);

		SashForm movable = new SashForm(this, SWT.FILL);
		movable.setOrientation(SWT.VERTICAL);
		movable.setLayout(new GridLayout(1, false));
		movable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 2));
		viewer = new TreeViewer(movable);
		t = viewer.getTree();
		t.setLayoutData(new GridData(SWT.FILL,SWT.FILL,true,true,3,1));
		tcp = new TCP();
        viewer.setContentProvider(tcp);
        tlp = new TLP();
        viewer.setLabelProvider(tlp);
        viewer.setInput(tcp.getInitial());
		t.setLinesVisible(true);
		t.setHeaderVisible(false);
		
		Device device = Display.getDefault();
		redHeaderColor = new Color(device,165,0,0);
		greenHeaderColor = new Color(device,80,200,80);
		
		failureDetails = new StyledText(movable, SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
		GridData gdFailureDetails = new GridData(SWT.FILL,SWT.FILL,true,false,3,1);
		gdFailureDetails.heightHint = 100;
		failureDetails.setLayoutData(gdFailureDetails);
		failureDetails.setEditable(false);
		movable.setWeights(new int[] {2, 1});
		
        viewer.addSelectionChangedListener(new ISelectionChangedListener(){
			@Override
			public void selectionChanged(SelectionChangedEvent e) {
				TestResult selected = ((TestResult)((IStructuredSelection)e.getSelection()).getFirstElement());
				if(selected != null)
					failureDetails.setText(selected.getMessage());
			}
        });
        
        this.addDisposeListener(new DisposeListener(){
			@Override
			public void widgetDisposed(DisposeEvent e) {
				redHeaderColor.dispose();
				greenHeaderColor.dispose();
				
				// Unnecessary if parent is already disposed :
			    t.dispose();
			    gradeText.dispose();
			    colorRectangle.dispose();
			    failureDetails.dispose();
			    testsLabel.dispose();
				errorsLabel.dispose();
				failuresLabel.dispose();
				
				ResultTreeView.this.removeDisposeListener(this);
			}
        });
    }
    
    private void setLabelsVisible(boolean visible){
		testsLabel.setVisible(visible);
		errorsLabel.setVisible(visible);
		failuresLabel.setVisible(visible);
    }

	void setResult(String result, String compilation, String date) {
		tcp.setContent(result,compilation, date);
		failureDetails.setText("");
		viewer.setInput(tcp.getInitial());
		viewer.expandAll();
	}
    
	void setGrade(String grade) {
		this.gradeText.setText(grade);
		StyleRange sr = new StyleRange();
		sr.fontStyle = SWT.BOLD;
		sr.start = 0;
		sr.length = grade.length();
		this.gradeText.setStyleRange(sr);
	}
	
	void clear(){
		this.setResult("", "", "");
		this.setGrade("");
		failureDetails.setText("");
	}
}
