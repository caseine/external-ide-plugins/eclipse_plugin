/*
 * Copyright 2018, 2019: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package caseine.vpl.views.vplview;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class TestResult {
	private enum TestStatus {
		OK,FAIL,ERROR
	}
	
	private TestResult parent;
	private ArrayList<TestResult> children;
	private TestStatus status;
	
	private String name;
	private StringBuilder message;
	
	
	public TestResult(TestResult parent, String text){
		this.parent = parent;
		this.children = new ArrayList<>();

		message = new StringBuilder();
		
		try {
			BufferedReader br = new BufferedReader(new StringReader(text));
			String line;
			line=br.readLine();
			name = (line==null ? "" : line);
			
			status = (line != null &&(line.startsWith(ResultTreeView.VplCompilationErrorsId) || line.startsWith(ResultTreeView.CompilationErrorsName))) ?
					TestStatus.ERROR : TestStatus.OK;
			
			line=br.readLine();
			if(line != null){
				if(line.startsWith(ResultTreeView.VplIncorrectTestId)){
					status = TestStatus.FAIL;
				}
				do{
					message.append(line + "\n");
					if(Pattern.compile("exception", Pattern.CASE_INSENSITIVE).matcher(line).find()){
						// The message is about an exception: mark the test as error.
						status = TestStatus.ERROR;
					}
				} while((line=br.readLine()) != null);
			}
			br.close();
		} catch (IOException e) {
			// Something went wrong while reading data - keep processing for other test results.
		}
		
		if(parent != null){
			parent.applyStatus(status);
			parent.children.add(this);
		}
	}
	
	/**
	 * Applies the status according to severity (applying OK if already in FAIL will let it in FAIL)
	 * @param status
	 */
	private void applyStatus(TestStatus status){
		if(this.status==TestStatus.OK || status==TestStatus.ERROR)
			this.status = status;
		
		if(parent != null)
			parent.applyStatus(this.status);
	}
	
	public boolean isOk(){
		return status==TestStatus.OK;
	}
	
	public boolean isFail(){
		return status==TestStatus.FAIL;
	}
	
	public boolean isError(){
		return status==TestStatus.ERROR;
	}
	
	public TestResult getParent(){
		return parent;
	}
	
	public boolean hasChildren(){
		return !children.isEmpty();
	}
	
	public Object[] getChildren(){
		return children.toArray();
	}
	
	public String getName(){
		return name;
	}
	
	public String getMessage(){
		return message.toString();
	}
}
