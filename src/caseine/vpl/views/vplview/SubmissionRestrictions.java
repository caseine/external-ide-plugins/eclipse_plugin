/*
 * Copyright 2018, 2019: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee
 * This software is part of the Caseine project.
 * This software was developed with the support of the following organizations:
 * Universit� Grenoble Alpes
 * Institut Polytechnique de Grenoble
 *
 *
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package caseine.vpl.views.vplview;

import java.text.DecimalFormat;
import java.util.Timer;
import java.util.TimerTask;

import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonValue;

import org.eclipse.core.resources.IProject;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;

import caseine.vpl.WebserviceClientFactory;
import vplwsclient.exception.MoodleWebServiceException;
import vplwsclient.exception.VplConnectionException;

/**
 * Class containing logic for remaining time and free evaluation display.
 * @author Titouan Mazier
 */
class SubmissionRestrictions {

    private long timeEnd;

    private int freeEvaluations;
    private int usedEvaluations;
    private String evaluationPenalty;

    private Label clockLabel;
    private Label evaluationsLabel;

    /* --- Common methods --- */

    /**
     * Schedule clock update and resync on regular basis.
     * @param view A SubmissionRestriction should have a 1 to 1 relationtionship with the view on which it will appear.
     * @param clockLabel        The label showing remaining time.
     * @param evaluationsLabel  The label showing remaining free evaluations.
     */
    SubmissionRestrictions(VplView view, Label clockLabel, Label evaluationsLabel) {
        this.clockLabel = clockLabel;
        this.evaluationsLabel = evaluationsLabel;

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Display.getDefault().asyncExec(() -> view.updateClock());
            }
        }, 0, 1000);
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Display.getDefault().asyncExec(() -> view.synchronize());
            }
        }, 0, 5 * 60 * 1000);
    }

    /**
     * Call the webservice to synchronize remaining time end evaluations with the vpl
     *
     * @param project The current project.
     * @return true if there is at least one restriction (time or number of evaluations).
     */
    boolean synchronize(IProject project) {
        boolean noRestrictions = true;
        try {
            JsonObject response = WebserviceClientFactory.createFromProject(project)
                    .callService("mod_vpl_subrestrictions");

            // Clock Sync
            JsonValue timeleft = response.get("timeleft");
            if (timeleft.getValueType() == JsonValue.ValueType.NUMBER) {
                JsonNumber jsonNumber = (JsonNumber) timeleft;
                timeEnd = System.currentTimeMillis() + ((jsonNumber.longValueExact() - 1) * 1000);
                noRestrictions = false;
            } else {
                timeEnd = -1; // There is no end for current vpl
            }

            // Evaluations sync
            JsonValue jsonFreeEvaluations = response.get("freeevaluations");
            if (jsonFreeEvaluations.getValueType() != JsonValue.ValueType.NUMBER) {
                freeEvaluations = 0;
            } else {
                freeEvaluations = ((JsonNumber) jsonFreeEvaluations).intValue();
                noRestrictions = false;
            }
            usedEvaluations = ((JsonNumber)response.get("nevaluations")).intValue();
            evaluationPenalty = response.get("reductionbyevaluation").toString();
            if (!evaluationPenalty.equals("null"))
                evaluationPenalty = evaluationPenalty.substring(1, evaluationPenalty.length() - 1);
            else
                evaluationPenalty = null;
        } catch (VplConnectionException | MoodleWebServiceException e) {
            timeEnd = -1;
            evaluationPenalty = null;
        }

        updateClockLabel();
        updateEvaluationsLabel();
        return !noRestrictions;
    }

    /* --- Clock --- */

    /**
     * Display an updated version of the timer.
     */
    void updateClockLabel() {
        if (clockLabel.isDisposed()) {
            return;
        }
        if (timeEnd == -1) {
            clockLabel.setText("");
            clockLabel.setToolTipText("");
            return;
        }
        long remainingTime = timeEnd - System.currentTimeMillis();
        if (remainingTime <= 0) {
            clockLabel.setText("Time is up !");
            clockLabel.setForeground(new Color(Display.getDefault(), 192, 0, 0));
            return;
        }
        clockLabel.setText("Time left : " + getFormatedTime((remainingTime / 1000), true));
        clockLabel.setToolTipText(getFormatedTime(remainingTime/1000, false));
        if (remainingTime > 60 * 60 * 1000)
            clockLabel.setForeground(new Color(Display.getDefault(), 0, 128, 0));
        else {
            double x = (double) remainingTime / (1 * 60 * 1000); // x goes from 1 to 0 during the last hour.
            int redGradient = (int) ((double) (- 896 * x * x * x + 1482.667 * x * x - 778.667 * x + 192));
            int greenGradient = (int)((double) (- 128 * x * x + 256 * x));
            clockLabel.setForeground(new Color(Display.getDefault(), (redGradient), greenGradient, 0));
        }
    }

    /**
     * Return formatted time as a human readable string.
     * @param time in seconds
     * @param approx true if the result can be troncated.
     */
    private String getFormatedTime(long time, boolean approx) {
        long days = time / 86400;
        long hours = (time % 86400) / 3600;
        long minutes = (time % 3600) / 60;
        long seconds = time % 60;
        if (!approx)
            return String.format("%d days %01d:%02d:%02d",days, hours, minutes, seconds);
        if (days >= 10)
            return String.format("%02d days", days);
        if (days == 1)
            return "1 day";
        if (days > 0)
            return String.format("%01d days", days);
        if (hours >= 10)
            return String.format("%02d hours", hours);
        if (hours == 1)
            return "1 hour";
        if (hours > 0)
            return String.format("%01d hours", hours);
        return String.format("%02d min %02d s", minutes, seconds);
        }

/* --- Free Evaluations --- */

    /**
     * Deprecated, manually increment the number of used evaluations (reduce the number of free evaluations)
     */
    void incrementUsedEvaluations() {
        this.usedEvaluations += 1;
        this.updateEvaluationsLabel();
    }

    /** Display an updated version of available evaluations.
     * evaluationsPenalty Can represent either a number, a percentage or null.
     */
    private void updateEvaluationsLabel() {
        if (evaluationPenalty == null) {
            evaluationsLabel.setText("");
            evaluationsLabel.setToolTipText("");
            return;
        }
        int remainingFreeEvals = freeEvaluations - usedEvaluations;
//        if (remainingFreeEvals > 0) {
            evaluationsLabel.setText("Evaluations : " + usedEvaluations + "/" + freeEvaluations + " -"+evaluationPenalty);
            evaluationsLabel.setToolTipText(usedEvaluations + " evaluations done\n"
                    +freeEvaluations+" free evaluations\n"
                    + "-"+evaluationPenalty+" penalty per additionnal evaluation\n"
                    + "Current penalty : "+((remainingFreeEvals<0)?computePenalty(-remainingFreeEvals):"0"));
            if (remainingFreeEvals > 0)
                evaluationsLabel.setForeground(new Color(Display.getDefault(), 0, 128, 0));
            if (remainingFreeEvals == 0)
                evaluationsLabel.setForeground(new Color(Display.getDefault(), 0, 0, 128));
            if (remainingFreeEvals < 0)
                evaluationsLabel.setForeground(new Color(Display.getDefault(), 192, 0, 0));
            return;
//        }
//        evaluationsLabel.setText("Penalty : " + computePenalty(remainingFreeEvals)+ " (-" + evaluationPenalty +" per evaluation)");
//        evaluationsLabel.setToolTipText(usedEvaluations + " evaluations done\n"+freeEvaluations+" free evaluations\n"+ computePenalty(-1)+" penalty per additionnal evaluation");
    }

    /**
     * Retrun a human readable version of the evaluation penalty multiplied by the parameter.
     * @param multiplyer    A multiplier which will be applyed to evaluationPenalty represented value.
     * @return  A string representing the result of the operation in the same format as evaluationPenalty.
     */
    private String computePenalty(int multiplyer) {
        DecimalFormat df = new DecimalFormat("0");
        df.setMaximumFractionDigits(2);
        if (evaluationPenalty.contains("%")){
            float percentage = Float.parseFloat(evaluationPenalty.substring(0, evaluationPenalty.length()-1));
            float value = ((float)multiplyer) * (int) percentage;
            return ((value <= 100)? df.format(value): 100) + "%";
        }
        float points = Float.parseFloat(evaluationPenalty);
        float value = (multiplyer) * points;
        return (df.format(value)) + " points";
    }
}
