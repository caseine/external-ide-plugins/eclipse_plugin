/*
 * Copyright 2018, 2019: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package caseine.vpl;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;

/**
 * Utility class for operations with the workbench (perspectives, views, parts, ...)
 * @author Astor Bizard
 */
public final class WorkbenchUtils {

	private WorkbenchUtils() {
		throw new IllegalStateException("Utility class");
	}

	/**
	 * Returns the project associated with the selection in the Project/Package Explorer.
	 * @param selection The selection from which to retrieve the associated project.
	 * @return The project associated with the selection, or {@code null} if no such project can be found.
	 */
	public static IProject getProjectFromSelection(ISelection selection){
		
		if(selection==null)
			return null;
		
		IResource resource;
		// Get the selection object from the TreeSelection item
		if (selection instanceof IStructuredSelection) {
			Object currentSelection = ((IStructuredSelection) selection).getFirstElement();
			if( !(currentSelection instanceof IAdaptable) )
				return null;
			else {
				resource = ((IAdaptable) currentSelection).getAdapter(IResource.class);
			}
		}
		else {
			// Selection is not a StructuredSelection -> the focus is probably on the editor
			IEditorPart activeEditor = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
			if (activeEditor == null)
				return null;
			else {
				resource = activeEditor.getEditorInput().getAdapter(IResource.class);
			}
		}
		if(resource == null)
			return null;
		else
			return resource.getProject();
	}
	
	/**
	 * Opens the specified perspective, after prompting the user for confirmation.
	 * @param perspectiveID The ID of the perspective to open.
	 * @param prompt The text to display as a prompt for confirmation. If {@code null}, no prompt will be displayed.
	 * @return {@code true} if the perspective was successfully open, {@code false} otherwise.
	 */
	public static boolean openPerspective(String perspectiveID, String prompt) {
		IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		String currentPerspective = window.getActivePage().getPerspective().getId();
		if(currentPerspective.equals(perspectiveID))
			return true;
		
		boolean confirmed;
		if(prompt==null)
			confirmed = true;
		else
			confirmed = MessageDialog.openConfirm(window.getShell(), "Open perspective", prompt);
		
		if(confirmed) {
			try{
				PlatformUI.getWorkbench().showPerspective(perspectiveID, window);
			} catch (WorkbenchException e) {
				ErrorDisplayer.displayAsDialog(e, window.getShell(), "Unable to open perspective");
				return false;
			}
			return true;
		}
		else
			return false;
	}
}
