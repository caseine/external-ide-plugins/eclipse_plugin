/*
 * Copyright 2018, 2019: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package caseine.vpl.swt;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

public class AlphaNumText {
	
	private Text text;
	private Label icon;
	private Label warning;
	
	private boolean allowEmpty;
	private boolean valid;
	private String name;
	
	private static final Image WARNING_ICON = PlatformUI.getWorkbench().getSharedImages().getImageDescriptor(ISharedImages.IMG_DEC_FIELD_WARNING).createImage();
	
	public AlphaNumText(Composite container, String label, String name, int numColumns, boolean allowEmpty, boolean hideEntry) {
		this.name = name;
		this.allowEmpty = allowEmpty;
		
		new Label(container, SWT.NONE).setText(label);
		
		int textStyle = SWT.BORDER;
		if (hideEntry) {
			textStyle |= SWT.PASSWORD;
		}
		text = new Text(container, textStyle);
		text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, numColumns-1, 1));
        
        icon = new Label(container, SWT.NONE);
        icon.setVisible(false);
        icon.setLayoutData(new GridData(SWT.RIGHT, SWT.FILL, false, false));
        
        warning = new Label(container, SWT.NONE);
        warning.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, numColumns-1, 1));
        warning.setVisible(false);

        text.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
            	check();
            	
            	boolean show = warning.getText().length()>0;
            	icon.setVisible(show);
            	warning.setVisible(show);
            }
        });
        
        check();
	}
	
	public AlphaNumText(Composite container, String label, String name, int numColumns, boolean allowEmpty) {
		this(container, label, name, numColumns, allowEmpty, false);
	}
	
	private void check() {
		warning.setText("");
    	boolean empty = text.getText().length() == 0;
    	boolean alphanum = text.getText().matches("[0-9A-Za-z]*");
    	if (empty) {
    		icon.setImage(WARNING_ICON);
    		warning.setText(name + " " + (allowEmpty ? "should" : "must") + " be specified.");
    	}
    	else if (!alphanum) {
    		icon.setImage(WARNING_ICON);
    		warning.setText(name + " must be alphanumeric.");
    	}
    	valid = (!empty || allowEmpty) && alphanum;
	}
	
	public boolean isValid() {
		return valid;
	}
	
	public String getText() {
		return text.getText();
	}
	
	public void setText(String text) {
		this.text.setText(text != null ? text : "");
	}
	
	public void setEnabled(boolean enabled) {
		text.setEnabled(enabled);
	}
	
	public void addModifyListener(ModifyListener listener) {
		text.addModifyListener(listener);
	}
	
	public void setFocus() {
		text.setFocus();
	}
	
}