/*
 * Copyright 2018, 2019: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package caseine.vpl.swt;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

/**
 * A dialog to set the VPL ID when we enable the nature.
 * Warranty : the result of getVPLID() is either an Integer, "", or null.
 * @author christophe Saint-Marcel (christophe.saintmarcel@velossity.fr)
 * @author Astor Bizard
 */
public class VplIDDialog extends Dialog {
	
    private AlphaNumText vplIDText;
    private String vplID;

    public VplIDDialog(Shell parentShell) {
        super(parentShell);
    }

    @Override
    protected Control createDialogArea(Composite parent) {
        Composite container = (Composite) super.createDialogArea(parent);
        container.getShell().setText("Caseine - Enabling Caseine");

        Label lblPrompt = new Label(container,SWT.NONE);
        lblPrompt.setText("Please enter the ID of the VPL you want to link this project with.");
        GridData gd = new GridData();
        gd.horizontalSpan = 2;
        lblPrompt.setLayoutData(gd);
        
        GridLayout layout = new GridLayout(2, false);
        layout.marginTop = 5;
        layout.marginLeft = 10;
        layout.marginRight = 10;
        container.setLayout(layout);
        
        vplIDText = new AlphaNumText(container, "&VPL ID: ", "VPL ID", 2, false);
		
        vplIDText.addModifyListener(new ModifyListener() {
            public void modifyText(ModifyEvent e) {
            	getButton(IDialogConstants.OK_ID).setEnabled(vplIDText.isValid());
            }
        });
        
        return container;
    }
    
    @Override
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true).setEnabled(false);
        createButton(parent, IDialogConstants.IGNORE_ID,IDialogConstants.IGNORE_LABEL, false);
    }

    @Override
    protected Point getInitialSize() {
        return new Point(450, 180);
    }
    
    @Override
    protected void buttonPressed(int buttonId) {
    	if (buttonId == IDialogConstants.OK_ID) {
			vplID = vplIDText.getText();
	    	this.setReturnCode(Window.OK);
    	} else {
			vplID = "";
	    	this.setReturnCode(Window.CANCEL);
    	}
    	this.close();
    }
    
    public String getVPLID() {
        return vplID;
    }
}