/*
 * Copyright 2018, 2019: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package caseine.vpl.marker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.ui.texteditor.AbstractDecoratedTextEditor;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.SimpleMarkerAnnotation;

import caseine.vpl.Activator;

/**
 * A factory used to set the markers and the annotations.
 * 
 * @author Christophe Saint-Marcel
 *
 */
public abstract class MarkerFactory {

	private MarkerFactory() {
		throw new IllegalStateException("Factory class");
	}

	// Marker ID
	public static final String MARKER = "caseine.comment.marker";

	// Annotation ID
	public static final String ANNOTATION = "caseine.comment.annotation";

	/**
	 * Creates a new marker on a resource, initializing its position and message attributes according to the specified region and message.
	 * @param res
	 * @param document
	 * @param region
	 * @param message
	 */
	public static IMarker createMarker(IResource res, IDocument document, IRegion region, String message) throws CoreException, BadLocationException {
		IMarker marker = null;
		// note: you use the id that is defined in your plugin.xml
		marker = res.createMarker(MARKER);
		marker.setAttribute(IMarker.MESSAGE, message);
		// compute and set char start and char end
		int start = region.getOffset();
		int end = region.getOffset() + region.getLength();
		marker.setAttribute(IMarker.CHAR_START, start);
		marker.setAttribute(IMarker.CHAR_END, end);
		marker.setAttribute(IMarker.LINE_NUMBER, document.getLineOfOffset(start) + 1);
		return marker;
	}

	/**
	 * Returns all the caseine markers in a given resource
	 * @param resource
	 */
	public static List<IMarker> findMarkers(IResource resource) {
		try {
			return Arrays.asList(resource.findMarkers(MARKER, true, IResource.DEPTH_ZERO));
		} catch (CoreException e) {
			Activator.log("An error occured while updating document markers.",e);
			return new ArrayList<>();
		}
	}

	/**
	 * Adds an annotation linked to the specified marker to the document, at the given region.
	 * @param document
	 * @param marker
	 * @param region
	 * @param cue
	 */
	public static Annotation addAnnotation(IDocument document, IMarker marker, IRegion region, AbstractDecoratedTextEditor editor) {

		IAnnotationModel iamf = getAnnotationModel(editor);

		// Note: The annotation type id specify that you want to create one of your annotations
		SimpleMarkerAnnotation ma = new SimpleMarkerAnnotation(ANNOTATION, marker);

		// Finally add the new annotation to the model
		iamf.connect(document);
		iamf.addAnnotation(ma, new Position(region.getOffset(), region.getLength()));
		iamf.disconnect(document);

		return ma;
	}

	/**
	 * Removes the annotation linked to the specified marker of the document.
	 * @param document
	 * @param marker
	 * @param cue
	 */
	public static void removeAnnotation(IDocument document, IMarker marker, AbstractDecoratedTextEditor editor) {

		IAnnotationModel iamf = getAnnotationModel(editor);

		Iterator<Annotation> annotationIterator = iamf.getAnnotationIterator();

		while (annotationIterator.hasNext()) {
			Annotation annotation = annotationIterator.next();
			if(annotation.getType().equals(ANNOTATION)) {
				SimpleMarkerAnnotation sma = (SimpleMarkerAnnotation) annotation;
				IMarker m = sma.getMarker();
				if (m.equals(marker)) {
					// Removes the annotation
					iamf.connect(document);
					iamf.removeAnnotation(annotation);
					iamf.disconnect(document);
				}
			}
		}
	}
	
	/**
	 * Returns the IAnnotationModel of the specified Editor
	 * @param cue
	 */
	private static IAnnotationModel getAnnotationModel(AbstractDecoratedTextEditor editor) {
		// The DocumentProvider enables to get the document currently loaded in the editor
		IDocumentProvider idp = editor.getDocumentProvider();
		// The IAnnotationModel enables to add/remove/change annotation to a Document loaded in an Editor
		return idp.getAnnotationModel(editor.getEditorInput());
	}
}
