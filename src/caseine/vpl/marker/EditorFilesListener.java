/*
 * Copyright 2018, 2019: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package caseine.vpl.marker;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.FindReplaceDocumentAdapter;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.ui.IPartListener2;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.texteditor.AbstractDecoratedTextEditor;

import caseine.vpl.Activator;
import caseine.vpl.nature.CaseineNature;

/**
 * A listener used to set the markers when a Java document is opened. It is called at
 * each start of the IDE and for each documents opened. 
 * @author Christophe Saint-Marcel
 *
 */
public class EditorFilesListener implements IPartListener2 {

	@Override
	public void partOpened(IWorkbenchPartReference arg0) {
		IWorkbenchPart part = arg0.getPart(false);
		if (part instanceof AbstractDecoratedTextEditor) {
			AbstractDecoratedTextEditor editor = (AbstractDecoratedTextEditor) part;

			IFile file = (IFile) editor.getEditorInput().getAdapter(IFile.class);
			if(file==null)
				// File is not part of any project - skip
				return;
			IProject project = file.getProject();
			try {
				if (project.hasNature(CaseineNature.NATURE_ID)) {
					
					String commentStart;
					switch(file.getName().substring(file.getName().lastIndexOf('.')+1)){
						case "py": commentStart = "###";
							break;
						case "java":
						default: commentStart = "///";
							break;
					}

					// This is the document we want to connect to. This is taken from
					// the current editor input.
					IDocument document = editor.getDocumentProvider().getDocument(editor.getEditorInput());

					FindReplaceDocumentAdapter documentAdapter = new FindReplaceDocumentAdapter(document);
					
					// Finds the regions with a CaseInE comment to set the markers in the document
					int offset = 0;
					IRegion region;
					
					while ((region = documentAdapter.find(offset, "(?m)" + commentStart + ".*$", true, true, false, true)) != null) {
						
						String message = documentAdapter.subSequence(region.getOffset() + commentStart.length(),
										region.getOffset() + region.getLength()).toString().trim();
						
						IMarker marker = MarkerFactory.createMarker(file, document, region, message);
						MarkerFactory.addAnnotation(document, marker, region, editor);

						offset = region.getOffset() + region.getLength();
					}

					document.addDocumentListener(new DocumentListener(file, editor, commentStart));
				}
			} catch (CoreException | BadLocationException e) {
				Activator.log("An error occured while updating document markers.",e);
			}
		}
	}

	@Override
	public void partClosed(IWorkbenchPartReference arg0) {
		// Cleans all markers as they are not persistent
		IWorkbenchPart part = arg0.getPart(false);
		if (part instanceof AbstractDecoratedTextEditor) {
			AbstractDecoratedTextEditor editor = (AbstractDecoratedTextEditor) part;

			IFile file = editor.getEditorInput().getAdapter(IFile.class);
			if(file==null)
				// File is not part of any project - skip
				return;
			
			for (IMarker marker : MarkerFactory.findMarkers(file)) {
				try {
					marker.delete();
				} catch (CoreException e) {
					Activator.log("An error occured while updating document markers.",e);
				}
			}
		}
	}

	@Override
	public void partVisible(IWorkbenchPartReference arg0) { /* Nothing special to do */ }
	@Override
	public void partHidden(IWorkbenchPartReference arg0) { /* Nothing special to do */ }
	@Override
	public void partInputChanged(IWorkbenchPartReference arg0) { /* Nothing special to do */ }
	@Override
	public void partActivated(IWorkbenchPartReference arg0) { /* Nothing special to do */ }
	@Override
	public void partDeactivated(IWorkbenchPartReference arg0) { /* Nothing special to do */ }
	@Override
	public void partBroughtToTop(IWorkbenchPartReference arg0) { /* Nothing special to do */ }
}