/*
 * Copyright 2018, 2019: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package caseine.vpl.marker;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentListener;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.Region;
import org.eclipse.ui.texteditor.AbstractDecoratedTextEditor;

import caseine.vpl.Activator;

/**
 * A listener used to set the markers when a Java document is modified.
 * This listener does only remove/recreate the markers around the document modification.
 * It works along implicit default marker updater and annotation model.
 * 
 * @author Christophe Saint-Marcel
 * @author Astor Bizard
 *
 */
public class DocumentListener implements IDocumentListener {

	private IFile file;
	private AbstractDecoratedTextEditor editor;
	
	private Map<IMarker,IRegion> currentMarkerPositions;
	
	private final String commentStart;

	public DocumentListener(IFile file, AbstractDecoratedTextEditor editor, String commentStart) throws CoreException {
		this.file = file;
		this.editor = editor;
		this.commentStart = commentStart;
		currentMarkerPositions = new HashMap<>();
		for (IMarker marker : MarkerFactory.findMarkers(file)) {
			int start = (int) marker.getAttribute(IMarker.CHAR_START);
			int length = (int) marker.getAttribute(IMarker.CHAR_END) - start;
			currentMarkerPositions.put(marker, new Region(start, length));
		}
	}

	@Override
	public void documentAboutToBeChanged(DocumentEvent changeEvent) {
		// First, we update the position of existing markers.
		// NOTE : we are only moving a virtual position (stored in currentMarkerPositions), to avoid conflict with the MarkerUpdater and AnnotationModel.
		//      This wouldn't work with a custom marker attribute either as such change would fire an event and modify the AnnotationModel behavior.
		
		// If the marker is within the change span, delete it (to eventually re-create it after).
		
		
		// In this method, the document is the old one (not modified yet).
		IDocument document = changeEvent.getDocument();
		try {
			for (IMarker marker : MarkerFactory.findMarkers(file)) {
				// Current marker position
				int markerStart = currentMarkerPositions.get(marker).getOffset();
				int markerEnd = markerStart + currentMarkerPositions.get(marker).getLength();
				// Change span (extended to whole lines)
				int changeStartLineStart = document.getLineInformationOfOffset(changeEvent.getOffset()).getOffset();
				IRegion changeEndLine = document.getLineInformationOfOffset(changeEvent.getOffset()+changeEvent.getLength());
				int changeEndLineEnd = changeEndLine.getOffset()+changeEndLine.getLength();
				if (changeEndLineEnd < markerStart) {
					// The change is entirely before the marker : just move it
					int diff = changeEvent.getText().length() - changeEvent.getLength();
					currentMarkerPositions.put(marker, new Region(markerStart+diff,currentMarkerPositions.get(marker).getLength()));
				}
				else if(changeStartLineStart <= markerEnd) {
					// The marker is within the change span - delete it
					marker.delete();
					MarkerFactory.removeAnnotation(document, marker, editor);
					currentMarkerPositions.remove(marker);
				}
			}
		} catch (CoreException | BadLocationException e) {
			Activator.log("An error occured while updating document markers.",e);
		}
	}
	
	@Override
	public void documentChanged(DocumentEvent changeEvent) {
		// Then, we create (or re-create) the markers by parsing the text around the modification.
		
		
		// In this method, the document is the new (modified) one.
		IDocument document = changeEvent.getDocument();
		try {
			// Change span (extended to whole lines)
			int changeStartLineStart = document.getLineInformationOfOffset(changeEvent.getOffset()).getOffset();
			IRegion changeEndLine = document.getLineInformationOfOffset(changeEvent.getOffset()+changeEvent.getText().length());
			int changeEndLineEnd = changeEndLine.getOffset()+changeEndLine.getLength();
			String changedLines = document.get(changeStartLineStart, changeEndLineEnd - changeStartLineStart);
			// Parse the change for caseine comments
			int offset = changedLines.indexOf(commentStart);
			while(offset != -1) {
				IRegion lineRegion = document.getLineInformationOfOffset(changeStartLineStart + offset);
				String lineContent = document.get(lineRegion.getOffset(), lineRegion.getLength());
				int lineIndex = lineContent.indexOf(commentStart);
				String message = lineContent.substring(lineIndex+commentStart.length()).trim();
				IRegion markerRegion = new Region(lineRegion.getOffset() + lineIndex, lineRegion.getLength() - lineIndex);
				IMarker marker = MarkerFactory.createMarker(file, document, markerRegion, message);
				MarkerFactory.addAnnotation(document, marker, markerRegion, editor);
				currentMarkerPositions.put(marker, markerRegion);
				offset = changedLines.indexOf(commentStart, offset + markerRegion.getLength());
			}
			
		} catch (CoreException | BadLocationException e) {
			Activator.log("An error occured while updating document markers.",e);
		}
	}
}
