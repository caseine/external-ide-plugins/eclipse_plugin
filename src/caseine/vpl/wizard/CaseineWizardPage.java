/*
 * Copyright 2018, 2019: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Universit� Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package caseine.vpl.wizard;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceDialog;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Link;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.PreferencesUtil;

import caseine.vpl.Activator;
import caseine.vpl.preferences.PreferenceConstants;
import caseine.vpl.swt.AlphaNumText;

/**
 * New project wizard page to prompt the user for VPL ID, token and other settings.
 * @author Astor Bizard
 */
public class CaseineWizardPage extends WizardPage {

	private Composite container;
	private AlphaNumText vplIDText;
	private AlphaNumText tokenText;
	private Button initialFilesRadio;
	private Button includeGILibCheckbox;
	
	private IPreferenceStore store;
	
	public CaseineWizardPage() {
		super(CaseineWizardPage.class.getSimpleName(), "Import settings", null);
	}
	
	@Override
	public void createControl(Composite parent) {
		container = new Composite(parent, SWT.NONE);
		container.setLayout(new GridLayout(2, false));
		
		store = Activator.getDefault().getPreferenceStore();
		
		vplIDText = new AlphaNumText(container, "&VPL ID: ", "VPL ID", 2, false);
		vplIDText.setFocus();
		tokenText = new AlphaNumText(container, "&Token: ", "Token", 2, false, true);
		
		ModifyListener listener = new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				updateStatus(!vplIDText.isValid() || !tokenText.isValid() ? "Some fields are empty or contain illegal values." : null);
			}
		};
		vplIDText.addModifyListener(listener);
		
		String storedToken = store.getString(PreferenceConstants.P_SECURITY_KEY);
		if (!storedToken.isEmpty()) {
			tokenText.setText(storedToken);
			tokenText.setEnabled(false);

			Link editTokenButton = new Link(container, SWT.NONE);
			editTokenButton.setLayoutData(new GridData(SWT.END, SWT.FILL, true, false, 2, 1));
			editTokenButton.setText("<a>Edit token</a>");
			editTokenButton.addSelectionListener(new SelectionAdapter(){
				@Override
				public void widgetSelected(SelectionEvent e) {
					PreferenceDialog dialog = PreferencesUtil.createPreferenceDialogOn(
						PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
						"caseine.vpl.preferences.CaseinePreferencePage",  
						null, null);
					dialog.open();
					tokenText.setText(store.getString(PreferenceConstants.P_SECURITY_KEY));
				}
			});
		}
		else {
			tokenText.addModifyListener(listener);
		}
		
		Composite radioContainer = new Composite(container, SWT.NONE);
		GridLayout gl = new GridLayout(2, false);
		gl.marginBottom = 1;
		radioContainer.setLayout(gl);
		GridData gd = new GridData();
		gd.horizontalSpan = 2;
		radioContainer.setLayoutData(gd);
		
		initialFilesRadio = addCheckbox(radioContainer, "&Initial files", SWT.RADIO, false,
				"Import required files in their initial state");
		addCheckbox(radioContainer, "&Current files", SWT.RADIO, true,
				"Import files from the last submission");
		
		Composite checkboxContainer = new Composite(container, SWT.NONE);
		GridLayout gl2 = new GridLayout(2, false);
		gl2.marginBottom = 1;
		checkboxContainer.setLayout(gl2);
		GridData gd2 = new GridData();
		gd2.horizontalSpan = 2;
		checkboxContainer.setLayoutData(gd2);
		includeGILibCheckbox = addCheckbox(checkboxContainer, " Include GI &Libraries", SWT.CHECK, false,
				"Include libraries used for graphic application in GI assignments");
		
		super.setControl(container);
		setPageComplete(false);
	}
	
	void updateStatus(String message) {
		setErrorMessage(message);
		setPageComplete(message == null);
	}
	
	private Button addCheckbox(Composite container, String label, int type, boolean checked, String tooltip) {
		Button checkbox = new Button(container, type);
		checkbox.setText(label);
		checkbox.setSelection(checked);
		checkbox.setToolTipText(tooltip);
		GridData gd = new GridData();
		gd.horizontalSpan = 2;
		checkbox.setLayoutData(gd);
		return checkbox;
	}
	
	public String getVplId() {
		return this.vplIDText.getText();
	}
	
	public String getToken() {
		return this.tokenText.getText();
	}
	
	public boolean getInitialCheckBox() {
		return this.initialFilesRadio.getSelection();
	}
	
	public boolean getIncludeGILibCheckBox() {
		return this.includeGILibCheckbox.getSelection();
	}
	
	@Override
	public boolean canFlipToNextPage() {
		// Override to avoid contacting the web-service on every modification
		// (this method is roughly the same as super without the call to getNextPage())
		return isPageComplete();
	}
	
	void finish() {
		store.setValue(PreferenceConstants.P_SECURITY_KEY, getToken());
	}
}