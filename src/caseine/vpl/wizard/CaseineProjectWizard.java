/*
 * Copyright 2018, 2019: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Universit� Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package caseine.vpl.wizard;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.core.resources.ICommand;
import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.junit.JUnitCore;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.WizardNewProjectCreationPage;
import org.eclipse.ui.wizards.newresource.BasicNewProjectResourceWizard;
import org.osgi.framework.Version;

import caseine.vpl.Activator;
import caseine.vpl.ErrorDisplayer;
import caseine.vpl.WorkbenchUtils;
import caseine.vpl.handlers.CaseineHandler;
import caseine.vpl.nature.CaseineNature;
import caseine.vpl.nature.NatureUtils;
import caseine.vpl.preferences.PreferenceConstants;
import caseine.vpl.properties.PropertyConstants;
import caseine.vpl.swt.PasswordDialog;
import caseine.vpl.views.vplview.VplView;
import vplwsclient.RestJsonMoodleClient;
import vplwsclient.VplFile;
import vplwsclient.exception.MoodleWebServiceException;
import vplwsclient.exception.PasswordException;
import vplwsclient.exception.VplConnectionException;
import vplwsclient.exception.VplException;

/**
 * Wizard (set of pages) to setup a new Caseine Project.
 * @author Astor Bizard
 */
public class CaseineProjectWizard extends BasicNewProjectResourceWizard {
	
	public static final String WIZARD_ID = "caseine.wizard.CaseineProjectWizard";
	
	private CaseineWizardPage settingsPage;
	private String vplPassword;

	private RestJsonMoodleClient moodleClient;
	
	private Composite parent;
	
	@Override
	public void addPages() {
		// Add our settings page before the standard ones
		settingsPage = new CaseineWizardPage();
		addPage(settingsPage);
		super.addPages();
	}
	
	@Override
	public void createPageControls(Composite parent) {
		this.parent = parent;
		// This override is here to delay the creation of the pages, so that we can set the project name from the VPL.
		// See : createPageControls(Composite) at
		//  https://help.eclipse.org/neon/index.jsp?topic=/org.eclipse.platform.doc.isv/reference/api/org/eclipse/jface/wizard/Wizard.html
		// See : setInitialProjectName(String) at
		//  https://help.eclipse.org/mars/index.jsp?topic=/org.eclipse.platform.doc.isv/reference/api/org/eclipse/ui/dialogs/WizardNewProjectCreationPage.html
	}

	@Override
	public IWizardPage getNextPage(IWizardPage page) {
		IWizardPage nextPage = super.getNextPage(page);
		if (page == settingsPage) {
			// We get the vplId and the token
			String vplId = settingsPage.getVplId();
			String token = settingsPage.getToken();
			
			IPreferenceStore store = Activator.getDefault().getPreferenceStore();
			String url = store.getString(PreferenceConstants.P_URL);
			moodleClient = new RestJsonMoodleClient(vplId, token, url);
			
			boolean retry;
			do {
				retry = false;
				try {
					// Retrieve project name
					// This is also a way to check connection, permissions, etc.
					String caseineProjectName = moodleClient.callService("mod_vpl_info").getString("name","");
					
					// Fill in the next page's project name
					((WizardNewProjectCreationPage) nextPage).setInitialProjectName(getValidAvailableProjectName(caseineProjectName));
				}
				catch(PasswordException e) {
					// The VPL requires a password. Prompt for it and retry.
					PasswordDialog pwDialog = new PasswordDialog(super.getShell());
					pwDialog.open();
					if(pwDialog.getReturnCode() == Window.OK) {
						vplPassword = pwDialog.getPassword();
						moodleClient.setPassword(vplPassword);
						retry = true;
					}
					else {
						return page;
					}
				}
				catch(MoodleWebServiceException | VplConnectionException e) {
					this.settingsPage.updateStatus(ErrorDisplayer.getDisplayableString(e));
					return page;
				}
			} while(retry);
		}
		return nextPage;
	}
	
	private static String getValidAvailableProjectName(String baseName) {
		// Remove any special character and multiple spaces from the project name
		final String legalChars = "a-zA-Z0-9 _\'-";
		String projName = Normalizer.normalize(baseName,Normalizer.Form.NFKD).replaceAll("[^"+legalChars+"]", "").replaceAll(" +", " ").trim();
		
		// Suffix the name with a version number if a project with a identical name already exists in the workspace
		IProject[] existingProjects = ResourcesPlugin.getWorkspace().getRoot().getProjects(IContainer.INCLUDE_HIDDEN);
		int maxExistingVersion=-1;
		for(IProject p : existingProjects) {
			if(!p.getName().startsWith(projName))
				continue;
			if(p.getName().equals(projName))
				maxExistingVersion = Math.max(maxExistingVersion,1);
			else {
				String version = p.getName().substring(projName.length());
				if(version.charAt(0)=='_') {
					try {
						maxExistingVersion = Math.max(maxExistingVersion,Integer.parseInt(version.substring(1)));
					} catch(NumberFormatException e) {
						// The suffix is not a version number, skip.
					}
				}
			}
		}
		if(maxExistingVersion!=-1)
			projName += "_" + (maxExistingVersion+1);
		
		return projName;
	}

	/**
	 * List of supported project types, along with associated nature and perspective
	 */
	public enum ProjectType {
		JAVA(JavaCore.NATURE_ID,"Caseine Java","caseine.vpl.perspectives.java"),
		PYTHON("org.python.pydev.pythonNature","Caseine Python","caseine.vpl.perspectives.python");
		
		private String natureID;
		private String perspectiveName;
		private String perspectiveID;
		ProjectType(String natureID, String perspectiveName, String perspectiveID){
			this.natureID = natureID;
			this.perspectiveName = perspectiveName;
			this.perspectiveID = perspectiveID;
		}
	}
	
	/**
	 * Scans files of a project (linked to the given client) to infer its language.
	 * @param moodleClient The client from which to read files.
	 * @return The language (type) of the project.
	 */
	public static ProjectType getProjectType(RestJsonMoodleClient moodleClient) {
		try {
			VplFile[] files = RestJsonMoodleClient.extractFiles(moodleClient.callService("mod_vpl_info"), "reqfiles");
			for(VplFile file : files) {
				String fileName = file.getFullName();
				int extensionPos = fileName.lastIndexOf('.');
				if (extensionPos!=-1) {
					String extension = fileName.substring(extensionPos);
					switch(extension) {
						case ".java": return ProjectType.JAVA;
						case ".py": return ProjectType.PYTHON;
						default: continue;
					}
				}
			}
			return ProjectType.JAVA;
		} catch (VplConnectionException | MoodleWebServiceException e) {
			return ProjectType.JAVA;
		}
	}
	
	@Override
	public boolean performFinish() {
		
		// Create the control of the reference page - necessary to finish the wizard without displaying it
		if(this.getPageCount()>2)
			super.getPages()[getPageCount()-1].createControl(parent);
		
		if(!super.performFinish())
			return false;
		
		settingsPage.finish();
		
		Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		
		IProject project = this.getNewProject();
		ProjectType projectType = getProjectType(moodleClient);
		
		try {
			// Note : needed to set this before adding the nature (because CaseineNature.configure() uses the property)
			project.setPersistentProperty(PropertyConstants.VPLID_PROPERTY, settingsPage.getVplId());
			
			project.setPersistentProperty(PropertyConstants.VPLPASSWORD_PROPERTY, vplPassword);
			
			NatureUtils.addNatures(project, projectType.natureID, CaseineNature.NATURE_ID);
			
			if(projectType == ProjectType.JAVA) {
				// Note : adding the Java project nature automatically associates the Java builder
				
				ArrayList<IClasspathEntry> libraries = new ArrayList<>();
				
				libraries.add(JavaRuntime.getDefaultJREContainerEntry());
				
				// Add src as source folder
				libraries.add(JavaCore.newSourceEntry(project.getFullPath().append(CaseineHandler.srcFolder)));
				
				// If Eclipse platform version is 4.7.2 or greater, use JUnit 5 ; else use JUnit 4
				if(Platform.getBundle("org.eclipse.platform").getVersion().compareTo(new Version(4,7,2)) >= 0)
					libraries.add(JavaCore.newContainerEntry(JUnitCore.JUNIT5_CONTAINER_PATH));
				else
					libraries.add(JavaCore.newContainerEntry(JUnitCore.JUNIT4_CONTAINER_PATH));
				
				JavaCore.create(project).setRawClasspath(libraries.toArray(new IClasspathEntry[0]), true, null);
			}
			else {
				// Add Python Builder
				IProjectDescription description = project.getDescription();
				ICommand builder = description.newCommand();
				builder.setBuilderName("org.python.pydev.PyDevBuilder");
				ArrayList<ICommand> builders = new ArrayList<>();
				builders.add(builder);
				builders.addAll(Arrays.asList(description.getBuildSpec()));
				description.setBuildSpec(builders.toArray(new ICommand[0]));
				project.setDescription(description, null);
			}
			
			boolean includeGILibs = settingsPage.getIncludeGILibCheckBox() && projectType == ProjectType.JAVA;
			
			// Import files from the VPL
			CaseineHandler.doCaseineCallPull(shell, project, moodleClient, settingsPage.getInitialCheckBox(), includeGILibs, "created");
			
		} catch (CoreException | IOException | VplConnectionException | MoodleWebServiceException e) {
			ErrorDisplayer.displayAsDialog(e, shell, "Error during project " + (e instanceof VplException ? "import" : "creation"));
			if(e instanceof CoreException) {
				Activator.log((CoreException)e);
			}
			return false;
		}
		
		if(projectType == ProjectType.PYTHON) {
			// Try to set python build path.
			// It requires PyDev functions, which we are not sure to have access to (if PyDev plugin is not present). Thus the dynamic access to classes and methods.
			try {
				Object pythonPath = Class.forName("org.python.pydev.plugin.nature.PythonNature").getMethod("getPythonPathNature",IProject.class).invoke(null, project);
				Class.forName("org.python.pydev.core.IPythonPathNature").getMethod("setProjectSourcePath",String.class).invoke(pythonPath, "/${PROJECT_DIR_NAME}");
			} catch(ClassNotFoundException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException e) {
				MessageDialog.openWarning(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Unable to setup project",
					"This kind of project depends on the PyDev plugin, which is missing on this Eclipse installation. You can install it from Help>Eclipse Marketplace.");
				return true;
			}
		}
		
		try {
			WorkbenchUtils.openPerspective(projectType.perspectiveID,
					"This kind of project is associated with the " + projectType.perspectiveName + " perspective. Do you want to open this perspective now?");
		} catch (UnsupportedOperationException e) {
			// The asked perspective was not available - stop here
			return true;
		}
		
		try {
			VplView view = VplView.findAndShow();
			view.recoverAndDisplayInfos(moodleClient);
		} catch (PartInitException e) {
			Activator.log(e);
		}
		
		return true;
	}
}