/*
 * Copyright 2018, 2019: Christophe Saint-Marcel, Astor Bizard, Nicolas Catusse, Lee Yee
 * This software is part of the Caseine project.
 * This software was developped with the support of the following organizations:
 * Université Grenoble Alpes
 * Institut Polytechnique de Grenoble
 * 
 * 
 * This file is part of Caseine Plugin for Eclipse.
 * Caseine Plugin for Eclipse is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Caseine Plugin for Eclipse is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Caseine Plugin for Eclipse.  If not, see <https://www.gnu.org/licenses/>.
 */

package caseine.vpl;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.swt.widgets.Shell;

import caseine.vpl.preferences.PreferenceConstants;
import vplwsclient.exception.MoodleWebServiceException;
import vplwsclient.exception.PasswordException;
import vplwsclient.exception.VplConnectionException;

/**
 * Utility class to display errors as human-edible messages to the user.
 * @author Astor Bizard
 */
public final class ErrorDisplayer {
	
	/**
	 * Return an explicit message related to an Exception (especially VplException),
	 * and adding eclipse-specific information on how to fix the problem.
	 */
	public static String getDisplayableString(Exception e) {
		String message = e.getMessage();
		if(e instanceof VplConnectionException) {
			message += "\nPlease check the webservice URL in Caseine preferences page.";
			IPreferenceStore store = Activator.getDefault().getPreferenceStore();
			if (store.getString(PreferenceConstants.P_EVALUATION_METHOD).equals(PreferenceConstants.EVALUATION_METHOD_LISTENWS)) {
				message += "\nIf you have issues with evaluation, you may also want to set evaluation method to compatibility mode (\"Wait for a few seconds\") in Caseine preferences page.";
			}
		}
		else if(e instanceof PasswordException) {
			message += "\nVPL Password can be set in project properties.";
		}
		else if(e instanceof MoodleWebServiceException) {
			message += "\nIf needed, you'll find VPL ID in project properties and token in Caseine preferences page.";
		}
		return message;
	}
	
	/**
	 * Display an Exception in a new MessageDialog. Should only be called inside GUI thread.
	 */
	public static void displayAsDialog(Exception e, Shell shell, String contextMessage) {
		MessageDialog.openError(shell, "Error", contextMessage + ":\n" + getDisplayableString(e));
	}
	
	private ErrorDisplayer() {
		throw new IllegalStateException("Utility class");
	}
}
