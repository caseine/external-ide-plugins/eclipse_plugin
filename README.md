# CaseineVPL

The VPL extension let you download, upload and evaluate the files required by a Moodle 'Virtual Programming Lab' (VPL) activity from the environment of Eclipse. Of course, you can also edit and run locally these files and take advantage of Eclipse for these tasks.

## Features

- simple setup using VPL id
- upload / evaluate tasks managed by a simple button click
- activity treeview for remote evaluation result

## Release Notes

The latest release is still a prototype that has not yet been tested at scale.

## Documentation

The documentation for [installation](https://moodle.caseine.org/mod/page/view.php?id=14015) and [usage](https://moodle.caseine.org/mod/page/view.php?id=14016) of this plugin can be found on [moodle.caseine.org](https://moodle.caseine.org).

## Overview

For exhaustive details about eclipse plugin development, you can refer to [the eclipse documentation](https://help.eclipse.org/latest/index.jsp?nav=%2F2).\
Here is a quick description of the different packages in `src/caseine/vpl`:

```bash
src/caseine/vpl             Contains a few utils classes
├── decorators              Used to add icon
├── handlers                Code for buttons
├── marker                  Code for comments over specific portions of code
├── nature                  Defines and apply the CaseineNature
├── perspectives            Code for new perspectives
├── preferences             Code for plugin settings
├── properties              Code for Persistent properties
├── swt                     Standard widget toolkit
├── views
│   ├── commentsview        View listing all comments 
│   └── vplview             View with vpl description and results of evaluations
└── wizard                  Code for new vpl wizard
```
